
// The Atlas namespace
Ext.ns('Atlas');

Atlas.mdaBaseUrl = '/info/mda';

// make abosulute path from relative
Atlas.makeMdaUrl = function(url) {
    if (url && url[0] != '/') {
        url = Atlas.mdaBaseUrl + '/' + url;
    }
    return url;
}

//-----------------------------------------------------------------
//
// A store representing the MDA run numbers for a partition.
//
//-----------------------------------------------------------------
Atlas.MDARunStore = Ext.extend(Ext.data.Store, {
       url: Atlas.mdaBaseUrl + '/db',
       baseParams: {nruns: '300'},
       restful: true,
       
       reader: new Ext.data.XmlReader({
           record: 'run'
        }, [
             { name: 'Number', mapping: '@number' },
             { name: 'Time', mapping: '@time' },
             { name: 'URL', mapping: '@url' }
        ])
});

//-----------------------------------------------------------------
//
// A store representing the OH providers in MDA.
//
//-----------------------------------------------------------------
Atlas.MDAProviderStore = Ext.extend(Ext.data.Store, {
       url: Atlas.mdaBaseUrl,
       restful: true,

       reader: new Ext.data.XmlReader({
           record: 'provider'
        }, [
             { name: 'Name', mapping: '@name' },
             { name: 'Server', mapping: '@server' },
             { name: 'URL', mapping: '@url' }
        ])
});

//-----------------------------------------------------------------
//
// A store representing the histogram tree in MDA.
//
//-----------------------------------------------------------------
Atlas.MDAHistogramTreeStore = Ext.extend(Ext.data.Store, {
        url: Atlas.mdaBaseUrl,
        restful: true,

        // the return will be XML, so lets set up a reader
        reader: new Ext.data.XmlReader({
               record: 'histo'
           }, [
               { name: 'Name', mapping: '@name'},
               { name: 'URL', mapping: '@url' }
           ])
});

//-----------------------------------------------------------------
//
// A store representing the histograms in MDA.
//
//-----------------------------------------------------------------
Atlas.MDAHistogramStore = Ext.extend(Ext.data.Store, {
    url: Atlas.mdaBaseUrl,
    restful: true,
    
    // the return will be XML, so lets set up a reader
    reader: new Ext.data.XmlReader({
        record: 'hfile'
    }, [
        { name: 'LBN', mapping: '@lb'},
        { name: 'URL', mapping: '@url' }
        ])
});

//-----------------------------------------------------------------
//
// A tree display of MDA providers and histograms.
//
//-----------------------------------------------------------------

Atlas.MDAHTreePanel = Ext.extend(Ext.tree.TreePanel, {

    root: { text: 'Select partition to start', children: [] },
    autoScroll: true,
    viewConfig: { forceFit: true, autoFill: true },

    // 
    constructor: function(config) {

        config = config || {};

        this.runStore = new Atlas.MDARunStore();
        this.providerStore = new Atlas.MDAProviderStore();
        this.htreeStore = new Atlas.MDAHistogramTreeStore();
        this.histoStore = new Atlas.MDAHistogramStore();

        Atlas.MDAHTreePanel.superclass.constructor.apply(this, arguments);

        if(config.changer) {
            config.changer.on('partitionChanged', this.partitionChanged, this);
        }
        this.addEvents({'histogramChanged' : true});
    },

    // delayed initialization for events
    initEvents: function() {
        Atlas.MDAHTreePanel.superclass.initEvents.apply(this, arguments);

        // show modal popups when loaders load their data
        var watch = new Ext.LoadMask(this.el, {msg: "Loading runs...", store: this.runStore});
        watch = new Ext.LoadMask(this.el, {msg: "Loading providers...", store: this.providerStore});
        watch = new Ext.LoadMask(this.el, {msg: "Loading histogram tree...", store: this.htreeStore});
        watch = new Ext.LoadMask(this.el, {msg: "Loading histogram data...", store: this.histoStore});
    },

    // new partition selected
    partitionChanged: function(partition_name, partition_url) {
        this.runStore.proxy.setUrl(partition_url, true);
        this.runStore.load({callback: this.getRuns, scope: this, partition_name: partition_name});
    },

    // fill top-level node with childrens which are runs
    getRuns: function(records, options, success) {
        var runs = []
        for(var i = 0; i < records.length; i++) {
            var run = records[i].get('Number');
            var time = records[i].get('Time');
            var url = Atlas.makeMdaUrl(records[i].get('URL'));

            runs[i] = {
                    text: run + ' (' + time + ')',
                    runNumber: run,
                    url: url,
                    cls: 'folder',
                    icon: 'images/run.png',
                    children: [], 
                    singleClickExpand: true, 
                    listeners: {expand: {fn: this.expandRun, scope: this}}
            };
        }

        this.setRootNode({ text: options.partition_name, cls: 'folder', children: runs, icon: 'images/partition.png' });
        this.getRootNode().expand(false, false);
        this.getRootNode().ensureVisible();

    },

    expandRun: function(node) {
        if (node.attributes['loaded']) return;

        this.providerStore.proxy.setUrl(node.attributes['url'], true);
        this.providerStore.load({
            callback: this.getProviders,
            scope: this,
            treeNode: node
        });
    },

    // get the list of providers/servers for given run
    getProviders: function(records, options, success) {

        var node = options.treeNode;

        var providers = [];
        var groups = {};
        for (var i = 0; i < records.length; i++) {
           var name = records[i].get('Name');
           var server = records[i].get('Server');
           var url = Atlas.makeMdaUrl(records[i].get('URL'));
           var provider = {
                   text: name,
                   server: server,
                   url: url,
                   cls: 'folder',
                   icon: 'images/provider.png',
                   children: [],
                   singleClickExpand: true,
                   listeners: { 'expand': { fn: this.expandProvider, scope: this }}
           };

           // group providers with - or _ in the name
           var split = name.search(/-|_/);
           if (split == -1) {
              providers.push(provider);
           } else {
              var pfx = name.substr(0, split);
              if (! groups.hasOwnProperty(pfx)) groups[pfx] = [];
              groups[pfx].push(provider);
           }
        }

        // move short groups to regular list, add log groups as nodes
        for (var pfx in groups) {
            if (! groups.hasOwnProperty(pfx)) continue;
            if (groups[pfx].length <= 5) {
                Array.prototype.push.apply(providers, groups[pfx]);
            } else {
                // sort them first
                groups[pfx].sort(function(a,b) { return (a.text < b.text ? -1 : 1); })
                var provider = {text: pfx + '...',
                    cls: 'folder',
                    children: groups[pfx],
                    singleClickExpand: true,
                    icon: 'images/provider.png'
                };
                providers.push(provider);
            }
        }

        providers.sort(function(a,b) { return (a.text < b.text ? -1 : 1); })
        node.appendChild(providers);
        node.attributes['loaded'] = true;
    },

    expandProvider: function(node) {
        if (node.attributes['loaded']) return;
        
        this.htreeStore.proxy.setUrl(node.attributes['url'], true);
        this.htreeStore.load({callback: this.getHistos, scope: this, treeNode: node});
    },

    // populate histogram tree from histogram records
    getHistos: function(records, options, success) {

        var node = options.treeNode;

        var tree = {};
        for(var i = 0; i < records.length; i++) {
            var name = records[i].get('Name');
            var url = Atlas.makeMdaUrl(records[i].get('URL'));

            var path = name.split('/');
            // drop first empty element
            if (path[0] == "") path.shift();

            var dir = tree;
            for(var j = 0; j < path.length-1; j++) {
                if (! dir.hasOwnProperty(path[j])) dir[path[j]] = {};
                dir = dir[path[j]];
            }
            dir[path[path.length-1]] = url;
        }

        // convert tree into Tree nodes
        var children = this.tree2nodes(tree);
        node.appendChild(children);
        node.attributes['loaded'] = true;
    },

    // recursive function to convert histogram tree into node tree
    tree2nodes: function(inp) {
        var out = [];
        for (var key in inp) {
            if (inp.hasOwnProperty(key)) {
                var data = inp[key];
                if (typeof data == "string") {
                    // "leaf" node (may not be a leaf if histogram has several LBs
                    out.push({
                        text: key,
                        cls: 'file',
                        icon: 'images/histo.png',
                        url: data,
                        children: [],
                        singleClickExpand: true,
                        listeners: {
                            'expand': {fn: this.expandHisto, scope: this},
                            'click': {fn: this.clickHisto, scope: this}
                            }
                    });
                } else {
                    // folder node, call ourself recursively to make children nodes
                    out.push({text: key, cls: 'folder', children: this.tree2nodes(data), singleClickExpand: true});
                }
            }
        }
        // sort this level by names
        out.sort(function(a,b) { return (a.text < b.text ? -1 : 1); });
        return out;
    },

    // need to expand (if possible) histogram
    expandHisto: function(node) {
        if (node.attributes['loaded']) return;
        
        this.histoStore.proxy.setUrl(node.attributes['url'], true);
        this.histoStore.load({callback: this.getLBS, scope: this, treeNode: node});
    },

    // simple click on a histogram
    clickHisto: function(node) {
        // complication here is that if we click then we also expand, expandHisto and 
        // clickHisto can be called in any order, and data may be loaded after clickHisto
        // is called. If data is not loaded yet then we set a special flag in attributes, 
        // otherwise we simpluy call showHisto (if it is single-LB histogram)
        if (node.attributes['loaded']) {
            if (node.attributes['imgurl']) this.showHisto(node);
        } else {
            node.attributes['showOnLoad'] = true;
        }
    },
    
    // populate histogram tree from histogram records
    getLBS: function(records, options, success) {
        var node = options.treeNode;
        var lbs = {};
        for(var i = 0; i < records.length; i++) {
            var lbn = records[i].get('LBN');
            var url = Atlas.makeMdaUrl(records[i].get('URL'));
            lbs[lbn] = url;
        }
        
        var keys = Object.getOwnPropertyNames(lbs);
        if (keys.length == 1) {
            // if only one unique LBN then update histogram node
            node.attributes['imgurl'] = lbs[keys[0]];
            node.leaf = true;
            if (node.attributes['showOnLoad']) this.showHisto(node);
        } else {
            // add LBs as children
            for(var i = 0; i < keys.length; i++) {
                node.appendChild({
                    text: keys[i],
                    leaf: true,
                    cls: 'file',
                    icon: 'images/histo.png',
                    imgurl: lbs[keys[i]],
                    listeners: {'click': {fn: this.showHisto, scope: this}}
                });
            }
            node.ui.getIconEl().src = 'images/multi-histo.png';
        }

        node.attributes['loaded'] = true;
    },
    
    showHisto: function(node) {
        var imgurl = node.attributes['imgurl'];
        if (imgurl) {
            this.fireEvent('histogramChanged', node.text, imgurl);
        }
    }

});
Ext.reg('atlas-mda-htree-panel', Atlas.MDAHTreePanel);
