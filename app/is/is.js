
var partition;
var server;
var class_name;

//
// OKS related functions
//

function show_oks_object(obj)
{
    $('#oksObject').html(obj[0] + ': ' + obj[1]);

    var txt = '';
    txt += '<li data-role="list-divider" role="header">Attributes</li>';
    for(i = 0; i < obj[2].length; i++) {
        txt += '<li data-theme="c"><div class="ui-grid-a"><span class="ui-block-a">' + obj[2][i].name + '</span><span class="ui-block-b">' + obj[2][i].value + '</span></div></li>';
    }

    txt += '<li data-role="list-divider" role="header">Relations</li>'

    for(i = 0; i < obj[3].length; i++) {
        var target =  obj[3][i].target;
        txt += '<li data-theme="c"><div class="ui-grid-a"><span class="ui-block-a">' + obj[3][i].name + '</span><span class="ui-block-b">' + ((target && target.length > 0) ? (target[0] + '@' + target[1]) : '') + '</span></div></li>';
    }
    $('#oks_values').html(txt).listview('refresh');
}

function get_oks_object(name)
{
    $.ajax('/info/current/' + partition + '/oks/' + class_name + '/' + name, {
            data: { format: 'json' },
                success: show_oks_object
        });
}

function process_oks_objects(objects)
{
    var txt = '';
    $('#OKSClassName').html(class_name);
    for(i = 0; i < objects.length; i++) {
        txt += '<li data-theme="c"><a href="#OKSObject" onclick="get_oks_object(' + "'" + objects[i] + "'" + ')">' + objects[i] + '</a></li>';
    }
    $('#oks_object_table').html(txt).listview('refresh');    
}

function list_oks_objects(className)
{
    class_name = className;
    $.ajax('/info/current/' + partition + '/oks/' + class_name, {
            data: { format: 'json' },
            success: process_oks_objects
                });
}

function process_classes(classes)
{
    var txt = '';
    for(i = 0; i < classes.length; i++) {
        txt += '<li data-theme="c"><a href="#OKSObjects" onclick="list_oks_objects(' + "'" + classes[i] + "'" + ')">' + classes[i] + '</a></li>'
    }
    $('#oks_class_table').html(txt).listview('refresh');
}

function list_classes()
{
    $.ajax('/info/current/' + partition + '/oks', {
            data: { format: 'json' },
                success: process_classes
                });
}

//
// OH related functions
//

function process_providers(providers)
{
    var txt = '';
    for(i = 0; i < providers.length; i++) {
        txt += '<li data-theme="c"><a href="#ISObjects" onclick="list_objects(' + "'" + providers[i].server + "','" + providers[i].provider + '.*' + "'" + ')">' + providers[i].provider + '</a></li>'
    }
    $('#oh_provider_table').html(txt);
    $('#oh_provider_table').listview('refresh');
}

function list_providers()
{
    $.ajax('/info/current/' + partition + '/oh', {
            data: { format: 'json' },
                success: process_providers
        });
}

//
// IS related functions
// 
function show_object(obj)
{
    $('#ObjectName').html(obj[0] + ': ' + obj[1] + ' ' + obj[2]);
    var txt = '';
    for(i = 3; i < obj.length; i++) {
        txt += '<li data-theme="c"><div class="ui-grid-a"><span class="ui-block-a">' + obj[i].name + '</span><span class="ui-block-b">' + obj[i].value + '</span></div></li>';
    }
    $('#is_value_table').html(txt);
    $('#is_value_table').listview('refresh');
}

function get_object(name)
{
    $.ajax('/info/current/' + partition + '/is/' + server + '/' + name, {
            data: { format: 'json' },
                success: show_object
                });
}

function get_histogram(name)
{
    $('#HistogramName').html(name);
    $('#image').html('<img src="/info/current/' + partition + '/oh/' + name + '">');
}

function process_objects(objects)
{
    var txt = '';
    $('#ISServerName').html(server);
    for(i = 0; i < objects.length; i++) {
        if(objects[i]['type'].indexOf('HistogramData') == 0)  {
            txt += '<li data-theme="c"><a href="#Histogram" onclick="get_histogram(' +  "'" + objects[i].name + "'" + ')">' + objects[i].name.replace(server + '.','') + '</a></li>';
        } else {
            txt += '<li data-theme="c"><a href="#Object" onclick="get_object(' + "'" + objects[i].name + "'" + ')">' + objects[i].name.replace(server + '.','') + '</a></li>';
        }
    }
    $('#is_object_table').html(txt);
    $('#is_object_table').listview('refresh');
}

function list_objects(srv, regex)
{
    server = srv;

    $.ajax('/info/current/' + partition + '/is/' + server, {
            data: { format: 'json', regex: regex || '.*' },
                success: process_objects
                });
}

function process_servers(servers)
{
    var txt = '';
    for(i = 0; i < servers.length; i++) {
        txt += '<li data-theme="c"><a href="#ISObjects" onclick="list_objects(' + "'" + servers[i].name + "'" + ')">' + servers[i].name + '</a></li>';
    }
    $('#is_server_table').html(txt);
    $('#is_server_table').listview('refresh');
}

function list_server(part)
{
    partition = part;

    $.ajax('/info/current/' + partition + '/is', {
            data: { format: 'json' },
                success: process_servers
                });
}

//
// Partition related functions
//

function process_partitions(response)
{
    var txt = ''
        for (i = 0; i < response.length; i++) {
            txt += '<li><a href="#ISServers" onclick="list_server(' + "'" + response[i] + "'" + ')" >' + response[i] + '</a></li>';
        }
    
    $('#partition_table').html(txt);
    $('#partition_table').listview('refresh');
}

$(document).ready(function () { 
        $.ajax('/info/current', { 
                data: { format: 'json' },
                success: process_partitions
                    })
            });

