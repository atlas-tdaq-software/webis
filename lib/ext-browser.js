Ext.onReady(function(){

    //
    // The panel showing the partitions.
    //
    var partition_panel = Ext.ComponentMgr.create({ 
        xtype: 'atlas-partition-panel',
        collapsible: true,
        region: 'west',
        split: true
    });

    var browser_panel = new Ext.ComponentMgr.create({
        xtype: 'panel',
        layout: 'border',
        split: true,
        items: [ partition_panel, 
                 { xtype: 'tabpanel',
                   region: 'center',
                   activeItem: 0,
                   title: 'Services',
                   items: [ { layout: 'fit', title: 'Information Service', items: { xtype: 'atlas-is-browser', changer: partition_panel}, closable: true },
                            { layout: 'fit', title: 'Histogram Service', items: { xtype: 'atlas-oh-tree-browser', changer: partition_panel }, closable: true },
                            { layout: 'fit', title: 'OKS', items: { xtype: 'atlas-oks-browser', changer: partition_panel }, closable: true },
                            { layout: 'fit', title: 'Process Manager', items: { xtype: 'atlas-pmg-browser', changer: partition_panel }, closable: true }
/*
                            { layout: 'fit', title: 'Histograms', items: { xtype: 'atlas-oh-browser', changer: partition_panel}, closable: true }
*/
                          ]
                 }]
    });

    partition_panel.getStore().load();

    var view = new Ext.Viewport({ items: browser_panel, layout: 'fit' });

});
