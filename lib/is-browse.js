
import { draw, redraw, parse, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';

const base = '../../../info/current/';

let partition = ''
let server = '';
let provider;
let server_source;
let object_source;

let acc = 1;

// Generate element nodes from HTML text
function gen(html) {
  const template = document.createElement('template');
  template.innerHTML = html.trim();
  return template.content.children[0];
}

function decode(s)
{
  return s.replace('&lt;', '<').replace('&gt;', '>');
}

// Get the partitions and fill the dropdown menu
function get_partitions() {
  const sel = document.querySelector('#partition');
  sel.innerHTML = '';
  sel.append(gen('<option>Choose a Partition...</option>'));
  fetch(base + '?format=json')
    .then((response) => response.json())
    .then((result) => {
      result.forEach((part) => sel.append(gen('<option value="' + part + '">' + part + '</option>')));
    });
}

// clear the actual data display (table or histogram)
function clear_object_display()
{
  // clear subscriptions
    if(object_source) {
      object_source.close();
      object_source = undefined;
    }

    // clear object/histogram section
    document.querySelector('#object-name').textContent = '';
    document.querySelector('#object-type').textContent = '';
    document.querySelector('#object-time').textContent = '';
    document.querySelector('#object-data').innerHTML = '';
    document.querySelector('#object-display').innerHTML = '';
}

function update_server_or_provider(server, subscription_url)
{
  // clear subscription
  if(server_source) {
    server_source.close();
    server_source = undefined;
  }

  clear_object_display();

  // clear object list
  const objs = document.querySelector('#objects');
  objs.innerHTML = '';

  // listen to creation/deletion, get initial set via subscribed events
  server_source = new EventSource(subscription_url);

  // an existing object, or a newly created on
  ['subscribed', 'created'].forEach(event_name => {
    server_source.addEventListener(event_name, (event) => {
      const obj = JSON.parse(event.data);
      const name = obj[0].replace(server + '.','');
      make_object_entry(name, obj[1], server);
    });
  });

  // an object was deleted
  server_source.addEventListener('deleted', (event) => {
    const name = JSON.parse(event.data)[0].replace(server + '.','');
    document.querySelector('#objects').childNodes.forEach((elem) => {
      if(elem.textContent == name) {
        elem.parentNode.removeChild(elem);
        return;
      }
    });
  });
}

// Make a new object/histogram entry and insert it at right position
function make_object_entry(name, typ, server)
{
  let newNode = gen('<button type="button"  class="list-group-item list-group-item-action" title="' + typ + '">' + name + '</button>');

  /*
  if(obj[1] == 'HistogramProvider') {
    newNode = gen(`
<div class="accordion" id="acc-parent-${acc}">
  <div class="accordion-item">
    <h4 class="accordion-header">
       <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-${acc}" title="HistogramProvider">${name}</button>
    </h4>
    <div class="accordion-collapse" id="acc-${acc}" data-bs-parent="#acc-parent-${acc}"></div>
  </div>
</div>`);
    acc += 1;
  }
  */

  const objects = document.querySelector('#objects');
  for(const node of objects.childNodes) {
    if(name < node.textContent) {
      node.parentNode.insertBefore(newNode, node);
      return;
    }
    if(name == node.textContent) {
      node.setAttribute('title', typ);
      return;
    }
  }
  objects.append(newNode);
}

function initialize()
{
  // Get the partition list at start
  get_partitions();

  // Refresh button
  document.querySelector('#reload').addEventListener('click', (event) => get_partitions());

  // Filter for IS server names
  document.querySelector('#server-filter').addEventListener('input', (event) => {
    const filter = document.querySelector('#server-filter').value;
    const rex = filter.split(' ').map((el) => new RegExp(el, 'i'));
    document.querySelector('#servers-accordion .show div').childNodes.forEach((server) => {
      if(filter == '' || rex.every((r) => server.textContent.search(r) != -1)) {
        server.style.display = '';
      } else {
        server.style.display = 'none';
      }
    });
  });

  // Filter for IS object names
  document.querySelector('#name-filter').addEventListener('input', (event) => {
    const filter = event.target.value;
    const rex = filter.split(' ').map((el) => new RegExp(el, 'i'));
    document.querySelector('#objects').childNodes.forEach((obj) => {
      if(filter == '' || rex.every((r) => obj.textContent.search(r) != -1)) {
        obj.style.display = '';
      } else {
        obj.style.display = 'none';
      }
    });
  });

  // Filter for IS object types
  document.querySelector('#type-filter').addEventListener('input', (event) => {
    const filter = event.target.value;
    const rex = new RegExp(filter, 'i');
    document.querySelector('#objects').childNodes.forEach((obj) => {
      if(filter == '' || obj.getAttribute('title').search(rex) != -1) {
        obj.style.display = '';
      } else {
        obj.style.display = 'none';
      }
    });
  });

  // Partition has changed
  document.querySelector('#partition').addEventListener('change', (event) => {
    partition = event.target.value;
    const srvs = document.querySelector('#servers');
    srvs.innerHTML = '';
    const prov = document.querySelector('#providers');
    prov.innerHTML = '';
    clear_object_display()

    document.querySelector('#objects').innerHTML = '';
    if(partition != '') {
      document.querySelector('#is-load').style.display = 'inline';
      document.querySelector('#oh-load').style.display = 'inline';
      fetch(base + partition + '/is?format=json')
        .then((response) => response.json())
        .then((servers) => {
          servers.forEach((server) => srvs.append(gen('<button type="button"  class="list-group-item list-group-item-action">' + server['name'] + '</button>')));
          document.querySelector('#is-load').style.display = 'none';
        });
      fetch(base + partition + '/oh?format=json')
        .then((response) => response.json())
        .then((providers) => {
          providers.forEach((provider) => prov.append(gen('<button type="button"  class="list-group-item list-group-item-action" title="' + provider['server'] +  '">' + provider['provider'] + '</button>')));
          document.querySelector('#oh-load').style.display = 'none';
        });
    }
  });

  // IS Server has changed
  document.querySelector('#servers').addEventListener('click', (event) => {
    event.target.parentElement.querySelectorAll('.active').forEach((el) => el.classList.remove('active'));
    event.target.classList.add('active');
    server = event.target.textContent;
    update_server_or_provider(server, base + partition + '/is/' + server + '/' + server + '\..*?multiple=1&listen=create&listen=delete&listen=subscribe');
  });

  // OH provider has changed
  document.querySelector('#providers').addEventListener('click', (event) => {
    event.target.parentElement.querySelectorAll('.active').forEach((el) => el.classList.remove('active'));
    event.target.classList.add('active');
    provider = event.target.textContent;
    server = event.target.getAttribute('title');
    update_server_or_provider(server, base + partition + '/is/' + server + '/' + server + '.' + provider + '\..*?multiple=1&listen=create&listen=delete&listen=subscribe');
  });

  // IS Object has changed
  document.querySelector('#objects').addEventListener('click', (event) => {
    event.target.parentElement.querySelectorAll('.active').forEach((el) => el.classList.remove('active'));
    event.target.classList.add('active');

    clear_object_display();

    const obj = event.target.textContent;
    const type = event.target.getAttribute('title');

    document.querySelector('#object-name').textContent = obj
    document.querySelector('#object-type').textContent = type;

    if(type.search('HistogramData') == 0) {
      // we only listen to updates and initial subscription event
      object_source = new EventSource(base + partition + '/is/' + server + '/' + server + '.' + obj + '?listen=update&listen=subscribe');

      const histo_url = base + partition + '/oh/' + server + '.' + obj + '?type=compact';

      document.querySelector('#object-data').style.display = 'none';
      // clear histo display
      const d = document.querySelector('#object-display');
      d.innerHTML = ''; // cleanup(d);
      d.style.display = '';

      ['updated', 'subscribed'].forEach((event_name) => {
        object_source.addEventListener(event_name, (event) => {
          const ev = JSON.parse(event.data);
          document.querySelector('#object-time').textContent = ev[2];
          fetch(histo_url)
            .then((response) => response.text())
            .then((result) => {
              redraw(d, parse(result), 'colz');
            });
        });
      });

    } else {

      // we fetch the object with full metadata on attributes
      fetch(base + partition + '/is/' + server + '/' + server + '.' + obj + '?format=json')
        .then((response) => response.json())
        .then((result)   => {

          document.querySelector('#object-name').textContent = obj
          document.querySelector('#object-type').textContent = type;
          document.querySelector('#object-time').textContent = result[2];

          const d = document.querySelector('#object-display');
          d.style.display = 'none';
          cleanup(d);

          const data = document.querySelector('#object-data');
          data.style.display ='';
          data.innerHTML = '';
          for(let i = 3; i < result.length; i++) {
            const node =gen('<tr><th title="' + result[i]['descr'] + '">' + result[i]['name'] + '</th><td>' + result[i]['value'] + '</td></tr>');
            node.querySelector('td').textContent = result[i]['value'];
            data.append(node);
          }

          // we update with the compact JSON format only
          object_source = new EventSource(base + partition + '/is/' + server + '/' + server + '.' + obj + '?format=compact');
          object_source.addEventListener('updated', (event) => {
            const obj = JSON.parse(event.data);
            document.querySelector('#object-time').textContent = obj[2];
            document.querySelector('#object-data').querySelectorAll('th').forEach((name) =>
              name.nextElementSibling.textContent = obj[3][name.textContent]
            );
          });
        });
    }
  });
}

window.addEventListener('load', initialize());
