
var base_url = '/info/current/ATLAS/is/'; //switch TDAQ to ATLAS when running on website, not locally
var dfsumName = 'DFSummary';

var update = 10000;
var updateintervalvar=0;
var flashinterval = 2000;

var dataurls=[
    ['DF/DF.'+dfsumName , dfsumName],
    ['RunCtrl/RunCtrl.RootController' , 'RCStateInfo'],
    ['DF/DF.RoIB.RoIB_status' , 'RoIB'],
    ['DF/DF.HLTSV.Events', 'HLTSV'],
    ['DF/DF.TopMIG-IS:HLT.Input','SFOngInput'],
    ['DF/DF.TopMIG-IS:HLT.info', 'DCM'],
    ['DF/DF.TopMIG-IS:HLT.Counters.Global','SFOngCounters'],
    ['RunParams/RunParams.RunParams' , 'RunParams'],
    ['RunParams/RunParams.GlobalBusy' , 'RunParams'],
    //   ['RunParams/RunParams.TrigConfSmKey' , 'TrigConfSmKey'],
    // ['RunParams/RunParams.TrigConfL1BgKey' , 'TrigConfL1BgKey'],
    //['RunParams/RunParams.TrigConfL1PsKey' , 'TrigConfL1PsKey'],
    //['RunParams/RunParams.TrigConfHltPsKey' , 'TrigConfHltPsKey'],
    //['RunParams/RunParams.Physics.L1PsKey' , 'TrigConfL1PsKey'],
    //['RunParams/RunParams.Physics.HltPsKey' , 'TrigConfHltPsKey'],
    //['RunParams/RunParams.Standby.L1PsKey' , 'TrigConfL1PsKey'],
    //['RunParams/RunParams.Standby.HltPsKey' , 'TrigConfHltPsKey']
];

var pgbarcolors=[//should switch back colors for now
    ['pgbaryw','#E680B2'],
    ['pgbarpi','#D699EB'],//
    ['pgbaror','#FE9A2E'],
    ['pgbarbl','#66C2FF'],//
    ['pgbargr','#81F781'],
    ['pgbarpurp','#FF88FF']
]
var controlstate=""
$(document).ready(function() { //runs this when document is ready to be manipulated
    $( ".progressbar" ).progressbar({
	value: 99
    });
    $(".ui-progressbar-value").css("background","#FFFFA6");
    $(".tooltip *").css('font-family','\"Tahoma", Geneva,sans-serif');
    jQuery.each($(".tt_parent"),function() {
	var tooltipid="#"+$(this).attr('tooltipid');
	$(this).tooltip({predelay: 300,position:"center right", tip:tooltipid}).dynamic({ bottom: { direction: 'down', bounce: true } });
    });
});
//
//$ ('#something' ) is a css selector. select the element with an ID of 'something'. $ ('li') selects all list items on page. $('ul li') selects list items that are in unordered lists. $('.something') selects all elements with a class of 'something'

$(function (){
  $("#refreshtime").change(function(){
      refreshinterval();
  });
});
function refreshinterval(){
    window.clearInterval(updateintervalvar);
    update=$("#refreshtime").attr("options")[$("#refreshtime").attr("selectedIndex")].value;
    updateintervalvar=window.setInterval(get_items,update);
}

function flash(){
     if(!$("#flashcheckbox").attr("checked")){
	 jQuery.each($(".progressbar").not($(".noerrorflash")),function(){
	if ($(this).progressbar("option","value")>=100) {
	    var oldcolor= "";//$(this).children(".ui-progressbar-value").css("background-color")
	    for ( var k=0, len=pgbarcolors.length; k<len; ++k ){
		if ($(this).hasClass(pgbarcolors[k][0])){
		    oldcolor=pgbarcolors[k][1];
		}
	    }
	    $(this).children(".ui-progressbar-value").stop().css("background-color", "#FF0000").animate({ backgroundColor: oldcolor}, 1500);
	    
	}

    });
    }
}
function get_items() {

    for ( var i=0, len=dataurls.length; i<len; ++i ){
	var testvar=dataurls[i][0];
	try {(function(j){webis.request(base_url +  dataurls[i][0], {type: dataurls[i][1]},  function(response) {
	    var values = webis.get_all_attributes(response.responseXML);
	    var test='[data-url="'+dataurls[j][0]+'"]';
	    jQuery.each($(test),function() {
		if($(this).attr('data-name')!=undefined){
		    var v = values[$(this).attr('data-name')]//  $(this).attr('data-name')];
		    if(v == undefined) { console.log($(this).attr('data-name')); return; }
		    if((v.search('\\.') != -1 && parseFloat(v).toFixed(2)!="NaN" )) v = parseFloat(v).toFixed(2);
                    if($(this).hasClass('progressbar')){
			if (!($(this).hasClass("updated"))){
			    for ( var k=0, len=pgbarcolors.length; k<len; ++k ){
				if ($(this).hasClass(pgbarcolors[k][0])){
				    $(this).children(".ui-progressbar-value").css("background",pgbarcolors[k][1])
				}
			    }
			    $(this).addClass("updated")
			}
			var fracfilled=1
			var maxconst=100
			var maxvar=1
			if ($(this).attr('data-name')=="EFIOsfoBrkConn"){
			    v=0;
			    if (values["EFIOsfoBrkConnInterval"]>values["EFIOsfoBrkConn"]){
				v=1;
			    }
			}
			if ($(this).attr('data-name')=="EFIOsfiBrkConn"){
			    v=0;
			    if (values["EFIOsfiBrkConnInterval"]>values["EFIOsfiBrkConn"]){
				v=1;
			    }
			}
			if($(this).attr('data-name')=="RCBusy"){
			    v=0;
			    boolbusy=values["RCBusy"]=="1";
			    boolfault=values["RCFault"]=="1";
			    if (boolbusy || boolfault){
				v=100;
			    }
			}
			if($(this).attr('data-name')=="DAQBackPressureStatus"){
			    if (v.substring(0,2)=="OK"){
				v=0;
			    }
			    else{
				v=1;
			    }
			}
			if($(this).attr('data-maxim-var')!=undefined){
			    maxvar = values[$(this).attr('data-maxim-var')];
			    maxconst=1
			}
			if ($(this).attr('data-maxim') != undefined){
			    maxconst=$(this).attr('data-maxim');
			    
			}
			if (maxconst!=0 && maxvar!=0){
			    fracfilled=100*v/(maxconst*maxvar)
			}
			else{
			    fracfilled=0
			}
			$(this).progressbar("value", parseFloat(fracfilled),true);
			
		    }
		    else {
			
			if ($(this).attr('data-maxim') != undefined){
			    var maxconst=$(this).attr('data-maxim');
			    v= parseFloat(v/maxconst).toFixed(2);
			}
			if ($(this).attr('data-name') =="Recording"){
			    $(this).css('color','black');
			    var rec=$(this).attr('data-name');
			    v=rec
			    if (values["Recording"]==1){
				$(this).css('color','red');
				var rec=$(this).attr('data-name');
				v=rec
			    }
			}
			if ($(this).attr('data-name')=="DCSStableBeam"){
			    v="No"
			    if (values["DCSStableBeam"]==1){
				v="Yes"
			    }
			}
			if ($(this).attr('data-name')=="ROIBOutChStatus"){
			    v=v.replace(' ---- ----','')
			}
			if ($(this).attr('data-name')=="state"){
			    controlstate=v
			}
			if ($(this).attr('data-name')=="RCBusy"){
			    v=controlstate+" ["
			    boolbusy=values["RCBusy"]=="1"
			    boolfault=values["RCFault"]=="1"
			    if (boolbusy){
				v+=" RCBusy;"
			    }
			    if (boolfault){
				v+=" Fault;"
			    }
			    v+="]"
			}
  			$(this).text(v);

		    }
		}
		else{

		    var textdump="";
		    for (var myprop in values){
			if(!(values[myprop] instanceof Function)){
			    var myval=values[myprop];
			    textdump=textdump+myprop+": "+myval+"<BR>";
			}
		    }
		    $(this).empty();
		    $(this).append(textdump);		    
		}
	    });
	    var test='[data-url-list="'+dataurls[j][0]+'"]';
	    jQuery.each($(test),function() {
		var stringarray= values[$(this).attr('data-name')];
		var output=stringarray.join($(this).attr('data-spacing'))
		$(this).empty();
		$(this).append(output);
	    });
	    test='[data-url-list-first="'+dataurls[j][0]+'"]';
	    jQuery.each($(test),function() {
		var stringarray= values[$(this).attr('data-name')];
		$(this).empty();
		$(this).append(stringarray[0])
	    });
	});}(i));}
	catch(err){
	    $("#error").text("Cannot connect to IS server")
	}

    }

    $("#updated").text(Date());
}

