
function select_tab()
{
    var items = document.getElementsByClassName('selected');
    for(var i = 0; i < items.length; i++) {
	var s = items[i].className.search('selected');
	items[i].className = items[i].className.substr(0,s) + 
	    items[i].className.substr(s+9);
    }
    this.className += ' selected';
}

function init_tab(name)
{
    if(name == undefined) name = 'menu';
    var menus = document.getElementsByClassName(name);
    for(var m = 0; m < menus.length; m++) {
      var items = menus[m].getElementsByTagName('li');
      for(var i = 0; i < items.length; i++) {
	items[i].onclick = select_tab;
      }
    }
}
