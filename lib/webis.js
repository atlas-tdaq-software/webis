
document.domain = 'cern.ch'

webis = {
    helper:  'https://atlasop.cern.ch/tdaq/web_is/utils/helper.html',
    base: '../../../info/current/',

    collect_values : function(values, isMulti) 
    {
        var result;
        if(isMulti) {
           result = [];
           for(var v = 0; v < values.length; v++) {
             if(values[v].childNodes.length > 0) {
                result[result.length] = values[v].childNodes[0].nodeValue;
              } else {
                result[result.length] = '';
              }
           }
        } else {
          if(values[0].childNodes.length > 0) {
              result = values[0].childNodes[0].nodeValue;
          } else {
             result = '';
          }
        }
        return result;
    },

    get_attribute : function(obj, name)
    {
        var attrs = obj.getElementsByTagName('attr');
        for(var i = 0; i < attrs.length; i++) {
            if(attrs[i].getAttribute('name') == name) {
                return webis.collect_values(attrs[i].getElementsByTagName('v'), 
                                            attrs[i].getAttribute('multi') == '1');
            }
        }
    },

    get_all_attributes : function(  obj)
    {
        var result = {};
        var attrs = obj.getElementsByTagName('attr');
        for(var i = 0; i < attrs.length; i++) {
            var name = attrs[i].getAttribute('name');
            var values = webis.collect_values(attrs[i].getElementsByTagName('v'), 
                                              attrs[i].getAttribute('multi') == '1');
            result[name] = values;
        }
        return result;
    },

    fill_values : function(obj, names, mapping, format_numbers) 
    {
       var values = webis.get_all_attributes(obj);
       for(var i = 0; i < names.length; i++) {
           var v;
           if(mapping && mapping[names[i]]) {
              v = values[mapping[names[i]]];
           } else {
              v = values[names[i]];
           }
           // format any floating point number to 2 digits after decimal point
           if(format_numbers && v.search('\\.') != -1) v = parseFloat(v).toFixed(2);
           document.getElementById(names[i]).innerHTML = v;
       }
    },

    request : function(url, params, success, failure)
    {
        var el = document.getElementById('webis');
        if(!el || !webis.loaded) {
           setTimeout( function() { webis.request(url, params, success, failure); }, 100);
        } else {
           el.contentWindow.query(url, params, success,failure);
        }
    },

    options: {},

    init: function()
    {
        if(window.location.host == 'pc-atlas-www.cern.ch') { 
            webis.helper = window.location.protocol + '//pc-atlas-www.cern.ch/tdaq/web_is/utils/helper.html'; 
        }

        if(window.location.search.length > 0) {
            var qs = window.location.search.substr(1).split('&');
            for(var i = 0; i < qs.length; i++) {
               var keyval = qs[i].split('=',2);
               webis.options[keyval[0]] = keyval[1];
             }
         }

        var el = document.createElement("iframe");
        el.setAttribute('id', 'webis');
        el.style.width = '0px';
        el.style.height = '0px';
        el.style.visibility = 'hidden';
        el.setAttribute('src', webis.helper); 
	if (el.attachEvent){
	    el.attachEvent("onload", function(){ webis.loaded = 1});
	} else {
	    el.onload = function(){ webis.loaded = 1};
	}

        //el.setAttribute('onLoad', "webis.loaded = 1;");
        document.body.appendChild(el);
    }

}



