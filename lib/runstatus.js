
var old_timeout;
var update_time = 60;

var base = '../../../info/'

function display_data(release, partition, status, active_time)
{
   new Ajax.Request(base + release + '/' + partition + '/is/RunParams/RunParams.RunParams', 
                    { method: 'get',
                      parameters: { type: 'RunParams' },
                      onSuccess: function(transport) {
                         var obj = transport.responseXML.getElementsByTagName('obj')[0];
                         var the_data = {};
                         var attrs = obj.getElementsByTagName('attr');
                         for(var j = 0; j < attrs.length; j++) {
                            var values = attrs[j].getElementsByTagName('v')[0].childNodes;
                            if(values.length > 0) { 
                              the_data[attrs[j].getAttribute('name')] = values[0].nodeValue;
                            }
                         }

                         var result = $('data');
                         var row = result.insertRow(-1);
                         row.insertCell(-1).innerHTML = '<b>'+partition+'<\/b>';
                         row.insertCell(-1).innerHTML = '<b><a href="../app/is.html?partition='+partition+'"> IS <\/a><a href="../app/oh.html?partition='+partition+'"> OH  <\/a> <a href="../app/oks.html?partition='+partition+'"> OKS <\/a><a href="../app/pmg.html?partition='+partition+'"> PMG <\/a><\/b>';
                         row.insertCell(-1).innerHTML = the_data['run_number'];
                         row.insertCell(-1).innerHTML = the_data['run_type'];
                         row.insertCell(-1).innerHTML = status;
                         row.cells[row.cells.length-1].className = status;
                         row.insertCell(-1).innerHTML = the_data['recording_enabled'];
                         row.insertCell(-1).innerHTML = the_data['timeSOR'];
                         row.insertCell(-1).innerHTML = the_data['timeEOR'];
                         row.insertCell(-1).innerHTML = active_time;
                      },
                      onFailure: function(transport) { 
                         var result = $('data');
                         var row = result.insertRow(-1);
                         row.insertCell(-1).innerHTML = '<b>'+partition+'<\/b>';
                         row.insertCell(-1).innerHTML = '<b><a href="../app/is.html?partition='+partition+'"> IS <\/a><a href="../app/oh.html?partition='+partition+'"> OH  <\/a> <a href="../app/oks.html?partition='+partition+'"> OKS <\/a><a href="../app/pmg.html?partition='+partition+'"> PMG <\/a><\/b>';
                         row.insertCell(-1).innerHTML = the_data['run_number'];
                         row.insertCell(-1).innerHTML = the_data['run_type'];
                         row.insertCell(-1).innerHTML = status;
                         row.cells[row.cells.length-1].className = status;
                         row.insertCell(-1).innerHTML = '???';
                         row.insertCell(-1).innerHTML = '???';
                         row.insertCell(-1).innerHTML = '???';
                         row.insertCell(-1).innerHTML = active_time;        
                     }});
}

function display_active_time(release, partition, status)
{
   new Ajax.Request(base + release + '/' + partition + '/is/RunParams/RunParams.RunInfo',
                    { method: 'get',
                      parameters: { type: 'RunInfo' },
                      onSuccess: function(transport) {
                         var attrs = transport.responseXML.getElementsByTagName('obj')[0].getElementsByTagName('attr');
                         for(var i = 0; i < attrs.length; i++) {
                            if(attrs[i].getAttribute('name') == 'activeTime') {
                              display_data(release, partition, status, attrs[i].getElementsByTagName('v')[0].childNodes[0].nodeValue);
                              break;
                            }
                         }
                      },
                      onFailure: function(transport) { display_data(release, partition, status, '???'); }
                     });       
}

function display_status(release, partition)
{  
   new Ajax.Request(base + release + '/' + partition + '/is/RunCtrl/RunCtrl.' + (partition == 'initial' ? 'DefaultRootController' : 'RootController'),
                    { method: 'get',
                      parameters: { type: 'RCStateInfo' },
                      onSuccess: function(transport) {
                         var attrs = transport.responseXML.getElementsByTagName('obj')[0].getElementsByTagName('attr');
                         for(var i = 0; i < attrs.length; i++) {
                            if(attrs[i].getAttribute('name') == 'state') {
                              display_active_time(release, partition, attrs[i].getElementsByTagName('v')[0].childNodes[0].nodeValue);
                              break;
                            }
                         }
                      },
                      onFailure: function(transport) { display_active_time(release, partition, '???'); }
                     });    
}

function display_partitions(release)
{
   new Ajax.Request(base + release, 
                    { method: 'get',
                      onSuccess: function(transport) {
                         var parts = transport.responseXML.getElementsByTagName('partition');
                         for(var i = 0; i < parts.length; i++) {
                             display_status(release, parts[i].getAttribute('name'));
                         }
                      },
                      onFailure: function(transport) { alert("display_partitions error"); }
                     });    
}

function show()
{
   var h = {};
   if(window.location.search.length > 1) { 
        var args = window.location.search.substring(1).split('&');
        for(var i = 0; i < args.length; i++) {
            var keyval = args[i].split('=',2);
            h[keyval[0]] = keyval[1];
        }
   }
   if(h.release) {
     display_partitions(h.release);
   } else {
     display_partitions('current');
   }
   $('last_update').innerHTML = new Date();
}

function update_now()
{  
   if(old_timeout) { clearTimeout(old_timeout); old_timeout = null; }
   $('data').innerHTML = '';
   show();
   if(update_time > 0) {
     old_timeout = setTimeout('update_now()', update_time * 1000);
   }
}
