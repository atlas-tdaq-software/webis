
Atlas.RCTreePanel = Ext.extend(Ext.tree.TreePanel, {
    title: 'Run Control',
    autoScroll: true,    
    viewConfig: { forceFit: true, autoFill: true },
    root: { nodeType: 'async',
            text: '<span></span> ' + (Atlas.options.top || 'RootController'),
            children: [],
            singleClickExpand: true,
            name:  Atlas.options.top || '',
            expandable: true,
            ctrl: Atlas.options.top_controller || Atlas.options.top || 'RootController'
           },
    contextMenu: new Ext.menu.Menu({
        items: [
                 {
                   text: 'Recent Logfiles',
                    handler: function(item, obj) {
                      var node = item.parentMenu.contextNode;
                      window.open('/partlogs/' + node.attributes['host'] + '/' + item.parentMenu.partition + '?name=' + node.attributes['name']);
                    }
                  },
                  {
                    text: 'Restart', handler: function(item, obj) { alert('You got to be kidding...'); }
                  }
                ]
            }),
    listeners: { 'contextmenu': function(node, e) {
                                  node.select();
                                  var c = node.getOwnerTree().contextMenu;
                                  c.contextNode = node;
                                  c.partition = node.getOwnerTree().partition;
                                  c.showAt(e.getXY());
                                }},

    partitionChanged: function(partition_name, partition_url) {
        this.getRootNode().removeAll();
        this.getRootNode().collapse();
        this.getRootNode().attributes['loaded'] = false;
        this.getRootNode().setText('<span></span> ' + (Atlas.options.top || 'RootController'));
        this.setTitle('Run Control for ' + partition_name);
        this.partition = partition_name;
        this.partition_url = partition_url;
        this.updateStates([this.getRootNode()]);
    },

    updateStates: function(nodes) {

        if(!this.partition) return;

        for(var n = 0; n < nodes.length; ++n) {
         if(nodes[n].attributes['ctrl']) {
           var this_node = nodes[n];
           Ext.Ajax.request({
                  url: this.partition_url + '/is/RunCtrl/RunCtrl.' + nodes[n].attributes['ctrl'],
                  method: 'GET',
                  params: { attr: 'state,fault,errorCode,errorDescription' },
                  success: function(response) {
                      var s = Ext.query('attr[name=state] v', response.responseXML);
                      var state = s.length > 0 ? s[0].childNodes[0].nodeValue : 'ABSENT';

                      s = Ext.query('attr[name=fault] v', response.responseXML);
                      var err = 'NOERROR';
                      var err_description = '';

                      if(s.length > 0 && (s[0].childNodes[0].nodeValue == 'True')) {
                          err = 'ERROR';
                          err_description = Ext.query('attr[name=errorCode] v', response.responseXML)[0].firstChild.nodeValue + ' : ' + Ext.query('attr[name=errorDescription] v', response.responseXML)[0].firstChild.nodeValue
                      }

                      var span = this.getUI().getTextEl();
                      span.firstChild.innerHTML = state;
                      span.firstChild.className = 'RCSTATE ' + state + ' ' + err;
                      span.firstChild.setAttribute('title', err_description);

                  }.createDelegate(nodes[n])
           });
           if(nodes[n].isExpanded()) {
             this.updateStates(nodes[n].childNodes);
           }
         } else {
           Ext.Ajax.request({
                  url: this.partition_url + '/is/PMG/PMG.' + nodes[n].attributes['host'] + '|' + nodes[n].attributes['name'],
                  method: 'GET',
                  params: { attr: 'state' },
                  success: function(response) {
                      var s = Ext.query('attr[name=state] v', response.responseXML);
                      var state = s.length > 0 && s[0].childNodes[0].nodeValue ? 'UP' : 'ABSENT';
                      var span  = this.getUI().getTextEl();
                      span.firstChild.innerHTML = state;
                      span.firstChild.className = 'RCSTATE ' + state;
                  }.createDelegate(nodes[n])
           });
         }
       }
     },

    expandSegment:  function(node) {

        if(node.attributes['loaded']) { this.updateStates(node.childNodes); return; }

        var self = this;

        Ext.Ajax.request({ 
           url:    this.partition_url + '/segment/' + (node.attributes['name'] == 'RootController' ? '' : node.attributes['name']),
           success: function(response) {

               var result = Ext.util.JSON.decode(response.responseText);

               node.attributes['host' ] = result['segment'][0]['host'];
               node.attributes['name' ] = result['segment'][0]['name'];
               node.attributes['ctrl' ] = result['segment'][0]['ctrl'];

               var children = [];               

	       for(var i = 0; i < result['resources'].length; ++i) {
	          children[children.length] = { text: '<span class="RCSTATE"></span><span title="'+result['resources'][i]['host']+'">' + result['resources'][i]['name'] + '</span>',
                                                cls: 'file', 
						leaf: true,
                                                ctrl: result['resources'][i]['ctrl'] || '',
                                                name: result['resources'][i]['name'],
                                                host: result['resources'][i]['host']
                                                 };
               }

	       for(var i = 0; i < result['applications'].length; ++i) {
	          children[children.length] = { text: '<span class="RCSTATE"></span><span title="'+result['applications'][i]['host']+'">' + result['applications'][i]['name'] + '</span>',
                                                cls: 'file', 
						leaf: true,
                                                ctrl: result['applications'][i]['ctrl'] || '',
                                                name: result['applications'][i]['name'],
                                                host: result['applications'][i]['host']
                                                 };
               }

	       for(var i = 0; i < result['template_applications'].length; ++i) {
	          children[children.length] = { text: '<span class="RCSTATE"></span><span title="'+result['template_applications'][i]['host']+'">' + result['template_applications'][i]['name'] + '</span>',
                                                cls: 'file', 
						leaf: true,
                                                ctrl: result['template_applications'][i]['ctrl'] || '',
                                                name: result['template_applications'][i]['name'],
                                                host: result['template_applications'][i]['host']
                                                 };
               }

	       for(var i = 0; i < result['subsegments'].length; ++i) {

	          children[children.length] = { text: '<span class="RCSTATE"></span><span title="'+result['subsegments'][i]['host']+'">' + result['subsegments'][i]['ctrl'] + '</span>',
                                                cls: 'folder', 
                                                children: [], 
						listeners: { expand: { fn: self.expandSegment, scope: self } },
                                                singleClickExpand: true,
                                                ctrl: result['subsegments'][i]['ctrl'],
                                                name: result['subsegments'][i]['name'],
                                                host: result['subsegments'][i]['host'],
                                                app:  result['subsegments'][i]['app']
                                                };
               }


               if(Atlas.options.infrastructure) {
                  for(var i = 0; i < result['infrastructure'].length; ++i) {

                     children[children.length] = { text:  '<span class="RCSTATE"></span><span title="'+result['infrastructure'][i]['host']+'"><i>' + result['infrastructure'][i]['name'] + '</i></span>',
                                                   cls: 'file', 
                                                   leaf: true,
                                                   name: result['infrastructure'][i]['name'],
                                                   host: result['infrastructure'][i]['host']
                                                   };
                  } 
               }

               this.appendChild(children);
               this.attributes['loaded'] = true;
               self.updateStates(this.childNodes);
           }.createDelegate(node),
           failure: function(response) { alert('Partition not available ?'); }
         });
        },

        update_interval: Atlas.options.update || 15,

        initComponent: function() {
            Atlas.RCTreePanel.superclass.initComponent.apply(this, arguments);
            if(this.initialConfig.changer) {
                this.initialConfig.changer.on('partitionChanged', this.partitionChanged, this);
            }
            this.getRootNode().on('expand', this.expandSegment, this);
            this.updateStates([this.getRootNode()]);
            setInterval(function() { this.updateStates([this.getRootNode()]) }.createDelegate(this), this.update_interval * 1000);
        }
});
Ext.reg('atlas-rc-tree-panel', Atlas.RCTreePanel);

