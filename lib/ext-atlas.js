
// The Atlas namespace
Ext.ns('Atlas');

//--------------------------------------------------
//
// A dictionary with options given in the URL.
//
//--------------------------------------------------
Atlas.options = Ext.urlDecode(window.location.search.slice(1));

Atlas.origin  = Atlas.options.origin || window.location.pathname.replace(/\/tdaq\/.*$/, '/info');
Atlas.release = Atlas.options.release || 'current';

//----------------------------------------------------
//
// Base URL to use to access the IS/OH/OKS information.
//
//----------------------------------------------------
Atlas.baseUrl = Atlas.origin + '/' + Atlas.release


//--------------------------------------------------
//
// A store representing all running partitions.
//
//--------------------------------------------------

Atlas.PartitionStore = Ext.extend(Ext.data.Store, {
        url: Atlas.baseUrl,
        restful: true,
        baseUrl: Atlas.baseUrl,
        reader: new Ext.data.XmlReader({
                record: 'partition'
                },[
                     { name: 'Name', mapping: '@name' }
                ])
});

//----------------------------------------
//
// Panel to display Atlas partitions.
//
//----------------------------------------

Atlas.PartitionPanel = Ext.extend(Ext.grid.GridPanel, {

        store: new Atlas.PartitionStore(),
        columns: [
            { header: "Name", width: 200, dataIndex: 'Name', sortable: true }
        ],
        filter: function(value) { this.getStore().filter('Name', new RegExp(value,"ig"), true, false); return true; },
        width: 150,
        title: 'Partitions',
	stripeRows: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,

        filter: function(value) { 
                if(value == '') {
                        this.getStore().clearFilter();
                }  else { 
                        this.getStore().filter('Name',value, true, false);}  
                        return true; 
        },

        constructor: function(config) {
                config = config || {}

                config.tools = [ { id: 'refresh', handler: function() { this.getStore().reload(); }, scope: this } ];
                config.bbar= [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter', 
                                validator:  this.filter.createDelegate(this) 
                              }];

                Atlas.PartitionPanel.superclass.constructor.apply(this, arguments);

                // customization
                this.addEvents({ 'partitionChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('partitionChanged', grid.getStore().getAt(el).get('Name'), grid.getStore().baseUrl + '/' + grid.getStore().getAt(el).get('Name'));
                });
        }
});
Ext.reg('atlas-partition-panel', Atlas.PartitionPanel);

//-----------------------------------------------------------------
//
// A store representing a list of IS servers for a given partition.
//
//-----------------------------------------------------------------
Atlas.ISServerStore = Ext.extend(Ext.data.Store, {
        url: Atlas.baseUrl,
        baseUrl : '',
        restful: true,

        partitionChanged: function(partition_name, partition_url)
        {
                this.baseUrl =  partition_url + '/is';
                this.proxy.setUrl(this.baseUrl, true);
                this.load();
        },

        reader: new Ext.data.XmlReader({
               record: 'server'
           }, [
               { name: 'Name', mapping: '@name'}
        ]),

        constructor: function(config) {
                Atlas.ISServerStore.superclass.constructor.apply(this, arguments);
                if(config && config.url) {
                    this.baseUrl = config.url;
                }
        }
});

//-----------------------------------------------------------------
//
// A panel showing a list of IS servers for a given partition.
//
//-----------------------------------------------------------------
Atlas.ISServerPanel = Ext.extend(Ext.grid.GridPanel, {
        columns: [
            {header: "Name", width: 200, dataIndex: 'Name', sortable: true }
        ],
        width: 200,
        title: 'Servers',
        stripeRows: true,
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,

        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        partitionChanged: function(partition_name, partition_url) {
                this.setTitle('Servers for ' + partition_name);
                this.getStore().partitionChanged(partition_name, partition_url);
        },

        constructor: function(config) {

                config = config || {}
                config.store = new Atlas.ISServerStore(config.url ? {url: config.url} : {});

                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];

                Atlas.ISServerPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('partitionChanged', this.partitionChanged, this);
                }

                // customization
                this.addEvents({ 'serverChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('serverChanged', grid.getStore().getAt(el).get('Name'), grid.getStore().baseUrl + '/' + grid.getStore().getAt(el).get('Name'));
                });
        }
});

Ext.reg('atlas-server-panel', Atlas.ISServerPanel);

//-----------------------------------------------------------------
//
// A store representing the list of IS objects.
//
//-----------------------------------------------------------------
Atlas.ISObjectStore = Ext.extend(Ext.data.Store, {

        url: Atlas.baseUrl,

        baseUrl: '',
        restful: true,

        serverChanged: function(server, server_url) {
               this.baseUrl = server_url;
               this.proxy.setUrl(this.baseUrl, true);
               this.load();
        },

        reader: new Ext.data.XmlReader({
               record: 'obj'
           }, [
               // set up the fields mapping into the xml doc
               // The first needs mapping, the others are very basic
               { name: 'Name', mapping: '@name'},
               { name: 'Type', mapping: '@type' },
               { name: 'Time', mapping: '@time' }
           ])        
});

//-----------------------------------------------------------------
//
// A panel showing the list of IS objects.
//
//-----------------------------------------------------------------
Atlas.ISObjectPanel = Ext.extend(Ext.grid.GridPanel, {
        columns: [
            {header: "Name", width: 100, dataIndex: 'Name', sortable: true, renderer: function(value) { return value.substr(value.search('\\.')+1); } },
            {header: "Type", width: 100, dataIndex: 'Type', sortable: true},
            {header: "Time", width: 100, dataIndex: 'Time', sortable: true }
        ],
        width: 300,
        title: 'Objects',
        stripeRows: true,
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,

        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        serverChanged: function(server_name, server_url) {
                this.setTitle('Objects for ' + server_name);
                this.getStore().serverChanged(server_name, server_url);
        },

        constructor: function(config) {

                config.store = new Atlas.ISObjectStore(config.url ? {url: config.url} : {});

                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];


                Atlas.ISObjectPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('serverChanged', this.serverChanged, this);
                }

                // customization
                this.addEvents({ 'objectChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('objectChanged', grid.getStore().getAt(el).get('Name'), grid.getStore().getAt(el).get('Type'), grid.getStore().baseUrl + '/' + grid.getStore().getAt(el).get('Name'));
                });
        }
});

Ext.reg('atlas-object-panel', Atlas.ISObjectPanel);


//-----------------------------------------------------------------
//
// A store representing one IS object.
//
//-----------------------------------------------------------------
Atlas.ISDataStore = Ext.extend(Ext.data.Store, {
        // load using HTTP
        url: Atlas.baseUrl,
        restful: true,

        baseUrl: '',

        objectChanged: function(name, type_name, object_url) {

               // if (type_name.search(/^HistogramData|^ProfileData/) == -1) {
                  this.baseUrl = object_url;
                  this.proxy.setUrl(this.baseUrl, true);
                  this.load({ params: { type: type_name } });
               // } else {
                  // document.images['histo-display'].src = grid.getStore().baseUrl.replace(/\/is\/.*/, '/oh/') + n;
               //}                
        },

        // the return will be XML, so lets set up a reader
        reader: new Ext.data.XmlReader({
               record: 'attr'
           }, [
               { name: 'Name', mapping: '@name'},
               { name: 'Multi', mapping: '@multi', defaultValue: '0' },
               { name: 'Value', convert: function(selection, node) {
                    var result = '';
                    var vs = node.getElementsByTagName('v');
                    for(var v = 0; v < vs.length; v++) {
                         if(result != '') result += ', ';
                         if(vs[v].childNodes.length > 0) {
                           result += vs[v].childNodes[0].nodeValue;
                         } 
                    }
                    return result;
               }},
               { name: 'Type', mapping: '@type' },
               { name: 'Description', mapping: '@descr' }
           ]),

        constructor: function(config) {
                config = config || {}
                if(config.url) {
                        this.baseUrl = config.url;
                }
        }
});

//-----------------------------------------------------------------
//
// A panel showing one IS object.
//
//-----------------------------------------------------------------
Atlas.ISDataPanel = Ext.extend(Ext.grid.GridPanel, {
        columns: [
            {header: "Name",  width: 100, dataIndex: 'Name', sortable: true}, 
            {header: "Value", width: 100,  dataIndex: 'Value', sortable: false },
            {header: "Description", width: 100,  dataIndex: 'Description', sortable: false, hidden: true },
            {header: "Type", width: 100,  dataIndex: 'Type', sortable: false, hidden: true },
            {header: "IsArray", width: 100,  dataIndex: 'Multi', sortable: false, hidden: true }
        ],
        width:  200,
        title: 'Data',
        stripeRows: true,
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,

        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        objectChanged: function(name, type_name, object_url) {
                this.setTitle(name + ' : ' + type_name);
                this.getStore().objectChanged(name, type_name, object_url);
		this.object_url = object_url;
        },

        constructor: function(config) {
                config = config || {};

                config.store = config.store || new Atlas.ISDataStore(config.url ? {url: config.url} : {});
                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];


                Atlas.ISDataPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('objectChanged', this.objectChanged, this);
                }
        }
});

Ext.reg('atlas-data-panel', Atlas.ISDataPanel);

//-----------------------------------------------------------------
//
// A store representing the OH providers.
//
//-----------------------------------------------------------------
Atlas.ProviderStore = Ext.extend(Ext.data.Store, {
       url: Atlas.baseUrl,
       baseUrl: Atlas.baseUrl,
       restful: true,

       partitionChanged: function(partition_name, partition_url)
        {
                this.baseUrl =  partition_url + '/oh';
                this.proxy.setUrl(this.baseUrl, true);
                this.load();
        },

       reader: new Ext.data.XmlReader({
           record: 'provider'
        }, [
             { name: 'Name', mapping: '@name' },
             { name: 'Server', mapping: '@server' }
        ])
});

//-----------------------------------------------------------------
//
// A panel showing the OH providers.
//
//-----------------------------------------------------------------
Atlas.ProviderPanel = Ext.extend(Ext.grid.GridPanel, {
        columns: [
            {header: "Name", width: 200, dataIndex: 'Name', sortable: true },
            {header: "Server", width: 200, dataIndex: 'Server', sortable: true, hidden: true }
        ],
        width: 200,
        title: 'Providers',
	stripeRows: true,
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,

        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        partitionChanged: function(partition_name, partition_url) {
                this.setTitle('Providers for ' + partition_name);
                this.getStore().partitionChanged(partition_name, partition_url);
        },

        constructor: function(config) {
                config = config || {};

                config.store = new Atlas.ProviderStore({url: config.url || Atlas.baseUrl} );
                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];


                Atlas.ProviderPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('partitionChanged', this.partitionChanged, this);
                }

                // customization
                this.addEvents({ 'providerChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('providerChanged', grid.getStore().getAt(el).get('Name'), grid.getStore().getAt(el).get('Server'), 
                                        grid.getStore().baseUrl.replace(/\/oh$/,'') + '/is/' + grid.getStore().getAt(el).get('Server'));
                });
        }
});

Ext.reg('atlas-provider-panel', Atlas.ProviderPanel);


//-----------------------------------------------------------------
//
// A store representing the histograms in one OH provider.
//
//-----------------------------------------------------------------
Atlas.HistogramStore = Ext.extend(Ext.data.Store, {
        url: Atlas.baseUrl,
        baseUrl: '',
        restful: true,

        providerChanged: function(provider, server, base_url) {
               this.baseUrl = base_url;
               this.proxy.setUrl(this.baseUrl, true);
               this.load({ params: { regex: provider + '\\..*'}});
        },

        // the return will be XML, so lets set up a reader
        reader: new Ext.data.XmlReader({
               record: 'obj'
           }, [
               // set up the fields mapping into the xml doc
               // The first needs mapping, the others are very basic
               { name: 'Name', mapping: '@name'},
               { name: 'Type', mapping: '@type' },
               { name: 'Time', mapping: '@time' }
           ])
});

//-----------------------------------------------------------------
//
// A panel showing the histograms in one OH provider.
//
//-----------------------------------------------------------------
Atlas.HistogramPanel = Ext.extend(Ext.grid.GridPanel, {
        columns: [
            {header: "Name", width: 200, dataIndex: 'Name', sortable: true, renderer: function(value) { return value.split('.', 3)[2]; }},
            {header: "Time", width: 100, dataIndex: 'Time', sortable: true, hidden: true },
            {header: "Type", width: 100, dataIndex: 'Type', sortable: true, hidden: true }
        ],
        width: 200,
	stripeRows: true,
        title: 'Histograms',
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,
        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        providerChanged: function(provider, server, base_url) {
                this.setTitle('Histograms for ' + provider);
                this.getStore().providerChanged(provider, server, base_url);
        },

        constructor: function(config) {

                config = config || {};
                config.store = new Atlas.HistogramStore({url: config.url ? config.url : Atlas.baseUrl }),
                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];


                Atlas.HistogramPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('providerChanged', this.providerChanged, this);
                }

                this.addEvents({ 'histogramChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('histogramChanged', grid.getStore().getAt(el).get('Name'), grid.getStore().baseUrl.replace(/\/is\/.*/,'/oh') + '/' + grid.getStore().getAt(el).get('Name'));
                });
        }
});

Ext.reg('atlas-histogram-panel', Atlas.HistogramPanel);


//-----------------------------------------------------------------
//
// A tree display of the providers and histograms.
//
//-----------------------------------------------------------------

Atlas.HistogramTreePanel = Ext.extend(Ext.tree.TreePanel, {
        root: { text: 'Histograms', children: [] },
        autoScroll: true,
        viewConfig: { forceFit: true, autoFill: true },

        //rootVisible: false,

        partitionChanged: function(partition_name, partition_url) {
                this.getRootNode().setText(partition_name);
                this.getRootNode().children = [];
                this.store.partitionChanged(partition_name, partition_url);
                this.partition = partition_name;
                this.partition_url = partition_url;
        },

        expandNode: function(node) {
                // check if first time
                if(node.attributes['loaded']) return;
                this.histoStore.proxy.setUrl(this.partition_url + '/is/' + node.attributes['server']); 
                this.histoStore.load({  params: { regex: node.text + '\\..*'}});
                this.node = node;
        },

        getProviders: function(store, records, options) {
             records.sort(function(a,b) { return (a.get('Name') < b.get('Name') ? -1 : 1); });
             var providers = [];
	     var next = 0;	
             for(var i = 0; i < records.length; i++) {
                var name = records[i].get('Name');
                var server = records[i].get('Server');
                var provider = { 
                        text: name, 
                        server: server, 
                        cls: 'folder', 
                        children: [], 
                        singleClickExpand: true, 
                        listeners: { 'expand': { fn: this.expandNode, scope: this }}};

                // now check for - or _ in the name
                var split = name.search(/-|_/);

                if(split == -1) {
                   providers[next++] = provider;
                } else {
                   split = name.split(name[split],2);
                   var found = false;
                   for(var j = 0; j < next; j++) {
                      if(providers[j].text == split[0] && providers[j].children.length > 0) {
                         providers[j].children[providers[j].children.length] = provider;
                         found = true;
	                 break;
                      }
                   }
                   if(!found) {
		      providers[next++] = {
                          text: split[0],
                          cls: 'folder',
                          children: [ { text: name, server: server, cls: 'folder', children: [], singleClickExpand: true, listeners: { 'expand': { fn: this.expandNode, scope: this }}} ],
                          singleClickExpand: true
                      };
                   }
                }
             }

             // now undo the sub-tree for single child elements
	     for(var i = 0; i < providers.length; ++i) {
                if(providers[i].children.length == 1) {
                   providers[i] = providers[i].children[0];
                } else if (providers[i].children.length > 1) {
                   providers[i].text += '...';
                }
             }

             this.setRootNode({ text: this.partition, cls: 'folder', children: providers });
             this.getRootNode().expand();
        },

        getHistograms: function(store ,records, options) {

                function get_dir(p, n) {
                        for(var i = 0; i < p.children.length; i++) {
                                if(p.children[i].text == n) return p.children[i];
                        }
                        p.children[p.children.length] = { text: n, cls: 'folder', children: [], singleClickExpand: true }
                        return p.children[p.children.length-1];
                }

                records.sort(function(a,b) { return (a.get('Name') < b.get('Name') ? -1 : 1); });

                root = { children: [] };

                for(var i = 0; i < records.length; i++) {
                        var name = records[i].get('Name');
                        var n = name.split('/');
			if (n.length == 1) { n = ['',n[0]]; }
                        var d = root;
                        for (var j = 1; j < n.length; j++) {
                                if (j < n.length - 1) {
                                        d = get_dir(d,n[j]);
                                } else {
                                        d.children[d.children.length] = { 
                                                text: n[j], 
                                                leaf: true, 
                                                cls: 'file',
                                                full_name: name,
                                                listeners: { 
                                                        'click': { 
                                                                fn: function(node) { this.fireEvent('histogramChanged', node.attributes['full_name'], this.partition_url + '/oh/' + node.attributes['full_name']); }, 
                                                                scope: this
                                                        }
                                                }
                                         }
                                }
                        }
                }

                this.node.attributes['loaded'] = true;
                this.node.appendChild(root.children);

        },

        constructor: function(config) {
                config = config || {};

                this.store = config.store || new Atlas.ProviderStore({url: config.url ? config.url : Atlas.baseUrl});
                this.store.on('load', this.getProviders, this);

                this.histoStore = new Atlas.HistogramStore({url: Atlas.baseUrl});
                this.histoStore.on('load', this.getHistograms, this);

                Atlas.HistogramTreePanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('partitionChanged', this.partitionChanged, this);
                }
                this.addEvents({ 'histogramChanged' : true });
        }
});
Ext.reg('atlas-histogram-tree-panel', Atlas.HistogramTreePanel);

//-----------------------------------------------------------------
//
// A panel showing one histogram.
//
//-----------------------------------------------------------------
Atlas.HistogramDisplay =  Ext.extend(Ext.BoxComponent,{

      autoEl: { tag: 'img', width: 692, height: 444 },

      histogramChanged: function(name, histo_url) {
         var w = this.ownerCt.getWidth();
         var h = this.ownerCt.getHeight();

         var hv = histo_url.split('?');
         this.histogram = hv[0];
         this.values    = hv[1] ? Ext.urlDecode(hv[1]) : {}; 
	 this.setSize(w, h);
         this.values.size = ''+ (w + 4) + "x" + (h + 28);
         this.getEl().dom.src = Ext.urlAppend(this.histogram,Ext.urlEncode(this.values));
      },

      getSrc: function() {
         return this.getEl().dom.src;
      },

      reload: function(obj, w, h) {
         this.setSize(w,h);
         if(this.histogram) {
            this.values.size = ''+ (w + 4) + "x" + (h + 28);
            this.getEl().dom.src = Ext.urlAppend(this.histogram,Ext.urlEncode(this.values));
         }
      },

     constructor: function(config) {

        Atlas.HistogramDisplay.superclass.constructor.apply(this, arguments);

        if(config.changer) {
                config.changer.on('histogramChanged', this.histogramChanged, this);
        }

        this.on('afterrender', function(obj) { 
                             obj.ownerCt.on('resize', this.reload.createDelegate(this)) 
                          });
     }
});

Ext.reg('atlas-histogram-display', Atlas.HistogramDisplay);

Atlas.HistogramControl = Ext.extend(Ext.form.FormPanel, {

            handle: function() {
              var bform = this.getForm();
              var values = bform.getValues();
              for(var v in values) {
                  var field = bform.findField(v);
                  if(field.initialConfig.emptyText == values[v]) {
                      delete values[v];
                  }
              }

              if(values.from && values.to) {
                 values.rangex=values.from+','+values.to;
              }

              if(values.yfrom && values.yto) {
                 values.rangey=values.yfrom+','+values.yto;
              }

              if(values.width && values.height) {
                 values.size=values.width+'x'+values.height;
              }

              delete values.from;
              delete values.to;
              delete values.yfrom;
              delete values.yto;
              delete values.width;
              delete values.height;

              // alert(Ext.urlEncode(values));
              if(this.display) {
                  var src = this.display.getSrc();
                  if(!src || src == '') return;

              /*
                  var w_owner = this.display.ownerCt.getWidth(true);
                  var h_owner = this.display.ownerCt.getHeight(true);
                  this.display.setWidth(w_owner);
                  this.display.setHeight(h_owner);
                  values.size = w_owner + 'x' + h_owner;
              */
                  src = src.split('?', 1);           
                  this.display.histogramChanged('', src + '?' + Ext.urlEncode(values));
              }
            },


            loadRoot : function() {
                if(this.display) {
                   var src = this.display.getSrc();
                   if(!src || src == '') return;
                   src = src.split('?', 1)[0] + '?type=root';
                   window.location = src;
                }
            },

            reset: function() {
                this.getForm().reset();
            },

            initComponent: function() {
                 this.width = 300;
                 this.height = 60;
                 this.defaults = { hideLabel: true, anchor: '50%', style: { margin: '2px'} };
                 this.layoutConfig = { columns: 6 }; // , tableAttrs: { style: { margin: '1em' }, } };
                 this.layout = 'table';
                 this.items = [
                         { xtype: 'textfield', tabIndex: 1, name: 'draw', emptyText: 'Draw Options' },
                         { xtype: 'textfield', tabIndex: 3, name: 'from', emptyText: 'X Range From', maskRe: /[-0-9.]/ },
                         { xtype: 'textfield', tabIndex: 4, name: 'to', emptyText: 'X Range To', maskRe: /[-0-9.]/ },
                         { xtype: 'checkbox',  tabIndex: 7, inputValue: '1', name: 'logx', boxLabel: 'LogX', handler: this.handle.createDelegate(this) },
                         { xtype: 'checkbox',  tabIndex: 8, inputValue: '1', name: 'logy', boxLabel: 'LogY', handler: this.handle.createDelegate(this) },
                         { xtype: 'checkbox',  tabIndex: 9, inputValue: '1', name: 'logz', boxLabel: 'LogZ', handler: this.handle.createDelegate(this) },
                         { xtype: 'textfield', tabIndex: 2, name: 'optstat', emptyText: 'Statistics Options' },
                         { xtype: 'textfield', tabIndex: 5, name: 'yfrom', emptyText: 'Y Range From', maskRe: /[-0-9.]/ },
                         { xtype: 'textfield', tabIndex: 6, name: 'yto', emptyText: 'Y Range To', maskRe: /[-0-9.]/ },
                         { xtype: 'button', tabIndex: 11, text: 'Reset', handler: this.reset.createDelegate(this) },
                         { xtype: 'button', tabIndex: 10, text: 'Reload', handler: this.handle.createDelegate(this) },
                         { xtype: 'button', tabIndex: 12, text: 'ROOT', handler: this.loadRoot.createDelegate(this) },

                 ];

                 Atlas.HistogramControl.superclass.initComponent.apply(this, arguments);

                 if(this.initialConfig.display) {
                    this.display = this.initialConfig.display;
                 }
            }
});

Ext.reg('atlas-histogram-control', Atlas.HistogramControl);


//-----------------------------------------------------------------
//
// A store representing all OKS classes for a partition.
//
//-----------------------------------------------------------------
Atlas.OksClassStore = Ext.extend(Ext.data.Store, {
       url: Atlas.baseUrl,
       baseUrl : '',
       restful: true,
       partitionChanged: function(partition_name, partition_url) { 
                this.baseUrl =  partition_url + '/oks';
                this.proxy.setUrl(this.baseUrl, true);
                this.load();
       }, 
       reader: new Ext.data.XmlReader({
           record: 'oks-class'
        }, [
             { name: 'Name', mapping: '@name' }
        ])
});

//-----------------------------------------------------------------
//
// A panel showing all OKS classes for a partition.
//
//-----------------------------------------------------------------
Atlas.OksClassPanel = Ext.extend(Ext.grid.GridPanel, {
        columns: [
            {header: "Name", width: 200, dataIndex: 'Name', sortable: true }
        ],
        width: 200,
        title: 'Classes',
	stripeRows: true,
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,
        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        partitionChanged: function(partition_name, partition_url) { 
                this.setTitle('Classes for ' + partition_name);
                this.getStore().partitionChanged(partition_name, partition_url);
		window.location.hash = '';
        },

        constructor: function(config) {
                config = config || {};
                config.store = new Atlas.OksClassStore(config.url ? {url: config.url} : {});
                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];

                Atlas.OksClassPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('partitionChanged', this.partitionChanged, this);
                }

                // customization
                this.addEvents({ 'classChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('classChanged', grid.getStore().getAt(el).get('Name'),  grid.getStore().baseUrl + '/' + grid.getStore().getAt(el).get('Name'));
                });
        }
});

Ext.reg('atlas-oks-class-panel', Atlas.OksClassPanel);

//-----------------------------------------------------------------
//
// A store representing the objects for one OKS class.
//
//-----------------------------------------------------------------
Atlas.OksObjectStore = Ext.extend(Ext.data.Store, {
       url: Atlas.baseUrl,
       baseUrl: '',
       restful: true,

       classChanged: function(class_name, class_url) { 
                this.baseUrl =  class_url; 
                this.proxy.setUrl(this.baseUrl, true);
                this.load();
       }, 

       reader: new Ext.data.XmlReader({
           record: 'obj'
        }, [
             { name: 'Name', mapping: '@name' },
             { name: 'Type', mapping: '@type' }
        ])
        
});

//-----------------------------------------------------------------
//
// A panel showing the objects for one OKS class.
//
//-----------------------------------------------------------------
Atlas.OksObjectPanel = Ext.extend(Ext.grid.GridPanel, {

        columns: [
            {header: "Name", width: 200, dataIndex: 'Name', sortable: true },
            {header: "Type", width: 200, dataIndex: 'Type', sortable: true }
        ],
        width: 200,
        title: 'Objects',
	stripeRows: true,
        viewConfig: { forceFit: true, autoFill: true },
        loadMask: true,

        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        classChanged: function(class_name, class_url) { 
                this.setTitle('Objects for ' + class_name);
                this.getStore().classChanged(class_name, class_url);
		window.location.hash = '';
        },

        constructor: function(config) {
                config = config || {};
                config.store = new Atlas.OksObjectStore(config.url ? {url: config.url} : {});
                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];

                Atlas.OksObjectPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('classChanged', this.classChanged, this);
                }

                // customization
                this.addEvents({ 'objectChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('objectChanged', grid.getStore().getAt(el).get('Name'),  grid.getStore().getAt(el).get('Type'), grid.getStore().baseUrl + '/' + grid.getStore().getAt(el).get('Name'));
                });
        }
});

Ext.reg('atlas-oks-object-panel', Atlas.OksObjectPanel);

//-----------------------------------------------------------------
//
// A store representing one OKS object.
//
//-----------------------------------------------------------------
Atlas.OksDataStore = Ext.extend(Ext.data.Store, {
       url: Atlas.baseUrl,
       restful: true,

       objectChanged: function(name, type, object_url) { 
                          this.baseUrl =  object_url;
                          this.proxy.setUrl(this.baseUrl, true);
                          this.load();
       },

       reader: new Ext.data.XmlReader({
           record: 'attr,rel'
        }, [
             { name: 'Name', mapping: '@name' },
             { name: 'Type', mapping: '@type' },
             { name: 'member',
               convert: function(selection, node) {
                 if(node.nodeName == 'attr') {
                    var vs = node.getElementsByTagName('val');
                    if(vs.length == 0) return '<i>None</i>';

                    if(node.getAttribute('multi') == 'False') {
                        return vs[0].childNodes[0] ? vs[0].childNodes[0].nodeValue : '';
                    }

                    var i = 0;
                    var result = [];
                    for(var v = 0; v < vs.length; v++) {
                       if(vs[v].childNodes.length > 0) {
                          result[i++] = '<option>' + vs[v].childNodes[0].nodeValue + '</option>';
                        }
                    }
                    return '<select size="' + Math.min(6,Math.max(2,result.length)) + '">' + result.join() + '</select>';

                 } else if(node.nodeName == 'rel') {

                    var vs = node.getElementsByTagName('target');
                    if(vs.length == 0) return '<i>None</i>';

                    var i = 0;
                    var result = []

                    if(node.getAttribute('multi') == 'False') {
                        return '<span class="oks_link"><span class="oks_name">' + vs[0].getAttribute('name') + '</span> :<i><span class="oks_type">' + vs[0].getAttribute('type') + '</span></i></span>';
                    }

                    for(var v = 0; v < vs.length; v++) {
                        var name = vs[v].getAttribute('name');
                        var type = vs[v].getAttribute('type');
                        result[i++] = '<option value="' + name + '@' + type + '">' + name + ' : <i>' + type + '</i></option>';
                    }
                    result.sort();
                    return '<select class="oks_link" size="'+ Math.min(6,Math.max(2,result.length)) + '">' + result.join() + '</select>';
                 } else {
                    alert('unexpected...');
                 }
                 return '';
               }
             },
             { name: 'Multi', mapping: '@multi' },
             { name: 'descr' }
        ])
});


//-----------------------------------------------------------------
//
// A panel showing one OKS object.
//
//-----------------------------------------------------------------
Atlas.OksDataPanel = Ext.extend(Ext.grid.GridPanel, {

        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        objectChanged: function(name, type, object_url) { 
                this.setTitle(name + ' : ' + type);
                this.getStore().objectChanged(name, type, object_url);
		// Ext.History.add('oks:'+name+'@'+type);
        },

        selectLink : function(grid, row, col, evt) {
                        var cell = grid.getView().getCell(row,col);
                        var lnk = cell.getElementsByClassName('oks_link');
                        if(lnk.length > 0) {
                            var url = grid.getStore().baseUrl.split('/').slice(0,-2).join('/');
                            if(lnk[0].tagName == 'SELECT') {
                                var value = lnk[0].options[lnk[0].selectedIndex].value.split('@');
                                url += '/' + value[1] + '/' + value[0];
                                this.objectChanged(value[0], value[1], url); 
                                // Ext.History.add('oks:' + value[0] + '@' + value[1]);
                            } else {
                                var name = lnk[0].getElementsByClassName('oks_name')[0].childNodes[0].nodeValue;
                                var type = lnk[0].getElementsByClassName('oks_type')[0].childNodes[0].nodeValue;
                                url += '/' + type + '/' + name;
                                this.objectChanged(name, type, url); 
                                //Ext.History.add('oks:' + name + '@' + type);
                            }
                        }
                     },

        historyChanged : function(token) {
            if(token.search(/^oks:/) != -1) {
                var url = this.getStore().baseUrl.split('/').slice(0,-2).join('/');
                var values = token.split(':')[1].split('@');
                url += '/' + values[1] + '/' + values[0];
                if(url != this.getStore().baseUrl) {
                    this.objectChanged(values[0], values[1], url);
                }
            }
        },

        initComponent: function() {
                this.columns = [
                    {header: "Name", width: 150, dataIndex: 'Name', sortable: true },
                    {header: "Value", width: 200, dataIndex: 'member' },
                    {header: "Description", width: 100, dataIndex: 'descr' },
                    {header: "Type", width: 100, dataIndex: 'Type', sortable: true, hidden: true },
                    {header: "Multiple", width: 100, dataIndex: 'Multi', sortable: false, hidden: true }
                ];
                this.width = 400;
                this.title = 'Data';
	        this.stripeRows = true;
                this.viewConfig = { forceFit: true, autoFill: true, enableRowBody: true };
                this.loadMask = true;

                this.store = new Atlas.OksDataStore({url: this.initialConfig.url ? this.initialConfig.url : Atlas.baseUrl });

                this.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];

                Atlas.OksDataPanel.superclass.initComponent.apply(this, arguments);

                if(this.initialConfig.changer) {
                        this.initialConfig.changer.on('objectChanged', this.objectChanged, this);
                }

                this.on('cellclick', this.selectLink, this);
                // Ext.History.on('change', this.historyChanged, this);
        }
});

Ext.reg('atlas-oks-data-panel', Atlas.OksDataPanel);


//-----------------------------------------------------------------
//
// A store representing the hosts for one partition.
//
//-----------------------------------------------------------------
Atlas.PMGHostStore = Ext.extend(Ext.data.Store, {

        url: Atlas.baseUrl,

        baseUrl: '',
        restful: true,

        partitionChanged: function(partition, partition_url) {
                this.baseUrl =  partition_url +'/is/PMG';
                this.proxy.setUrl(this.baseUrl, true);
                this.load({ 
                        params: { type: 'PMGPublishedAgentData', regex: 'AGENT_.*' }
                });
        },

       reader: new Ext.data.XmlReader({
           record: 'obj'
        }, [
             { name: 'Name', mapping: '@name' }
        ])
});

//-----------------------------------------------------------------
//
// A panel showing the hosts for one partition.
//
//-----------------------------------------------------------------
Atlas.PMGHostPanel = Ext.extend(Ext.grid.GridPanel, {

        columns: [
            {header: "Name", width: 200, dataIndex: 'Name', sortable: true, renderer: function(value) { return value.substr(value.search('PMG_AGENT_') + 11); } }
        ],

        width: 200,
        title: 'Hosts',
	stripeRows: true,
        viewConfig: { forceFit: true, autoFill: true },
        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },
        loadMask: true,

        partitionChanged: function(partition, partition_url) {
                this.setTitle('Hosts for '+ partition);
                this.getStore().partitionChanged(partition, partition_url);
        },

        constructor: function(config) {
                config = config || {};
                config.store = config.store || new Atlas.PMGHostStore(config.url ? {url: config.url} : {});
                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];

                Atlas.PMGHostPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('partitionChanged', this.partitionChanged, this);
                }

                // customization
                this.addEvents({ 'hostChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('hostChanged', this.getStore().getAt(el).get('Name'), this.getStore().baseUrl);
                });
        }
});

Ext.reg('atlas-pmg-host-panel', Atlas.PMGHostPanel);

//-----------------------------------------------------------------
//
// A store representing the processes for one partition.
//
//-----------------------------------------------------------------
Atlas.PMGProcessStore = Ext.extend(Ext.data.Store, {

        url: Atlas.baseUrl,

        baseUrl: '',
        restful: true,

        hostChanged: function(name, base_url)
        {
                this.baseUrl =  base_url;
                this.proxy.setUrl(this.baseUrl, true);
                this.load({ 
                        params: { type: 'PMGPublishedProcessData', regex: name.replace('PMG.AGENT_','') + '.*' }
                });
        },

        reader: new Ext.data.XmlReader({
           record: 'obj'
        }, [
             { name: 'Name', mapping: '@name' },
             { name: 'Type', mapping: '@type' }
        ])
});

//-----------------------------------------------------------------
//
// A panel showing the processes for one partition.
//
//-----------------------------------------------------------------
Atlas.PMGProcessPanel = Ext.extend(Ext.grid.GridPanel, {

        columns: [
            {header: "Name", width: 200, dataIndex: 'Name', sortable: true, renderer: function(value) { return value.substr(value.search(/\|/) + 1); } }
        ],

        width: 200,
        title: 'Processes',
	stripeRows: true,
        viewConfig: { forceFit: true },
        loadMask: true,
        filter: function(value) { this.getStore().filter('Name',value, true, false); return true; },

        hostChanged: function(name, base_url) {
                this.setTitle('Processes for '+ name);
                this.getStore().hostChanged(name, base_url);
        },

        constructor: function(config) {
                config = config || {};
                config.store = config.store || new Atlas.PMGProcessStore(config.url ? {url: config.url} : {});
                config.bbar = [{
                                xtype: 'textfield', 
                                emptyText: 'Enter Filter',
                                validator: this.filter.createDelegate(this)
                               }];

                Atlas.PMGProcessPanel.superclass.constructor.apply(this, arguments);

                if(config.changer) {
                        config.changer.on('hostChanged', this.hostChanged, this);
                }

                // customization
                this.addEvents({ 'objectChanged' : true });
                this.on('rowclick', function(grid, el, evt) {
                        this.fireEvent('objectChanged', this.getStore().getAt(el).get('Name'), 'PMGPublishedProcessData', this.getStore().baseUrl + '/' + this.getStore().getAt(el).get('Name'));
                });
        }
});

Ext.reg('atlas-pmg-process-panel', Atlas.PMGProcessPanel);

//-----------------------------------------------------------------
//
// A panel showing one process.
//
//-----------------------------------------------------------------
Atlas.PMGDataPanel = Ext.extend(Atlas.ISDataPanel, {
        width: 400,

        constructor: function(config) {
                config = config || {};
                config.store = config.store || new Atlas.ISDataStore({url: config.url});
                Atlas.PMGDataPanel.superclass.constructor.apply(this, arguments);
		this.getColumnModel().setRenderer(1, function(value, meta, rec, ri, ci, store) {
			var n = rec.get('Name');
                        if(n == 'std_out' || n == 'std_err') {
 			  var h = this.object_url.split('/').slice(-1)[0].split('|')[0].substr(4);
			  var f = value.split('/').slice(-2).join('/');
			  return '<a target="_BLANK" href="/partlogs/' + h + '/' + f + '">' + value + '</a>';
                        } else { 
                          return value;
                        } 
                }.createDelegate(this));
        }
});
Ext.reg('atlas-pmg-data-panel', Atlas.PMGDataPanel);

Atlas.ISBrowser = Ext.extend(Ext.Panel, {

        setPartition: function(partition_name) {
            this.getComponent('servers').partitionChanged(partition_name, Atlas.baseUrl + '/' + partition_name);
        },

        initComponent: function() {

            this.layout = 'border';
            this.items = [
                {
                        xtype: 'atlas-server-panel',  
                        itemId: 'servers',
                        changer: this.initialConfig.changer,
                        region: 'west' , 
                        url: this.initialConfig.url,
                        split: true, 
                        collapsible: true
                   },
                   { 
                        xtype: 'atlas-object-panel',  
                        itemId: 'objects',
                        region: 'center'
                   },
                   { 
                        xtype: 'atlas-data-panel',    
                        itemId: 'data',
                        region: 'east', 
                        split: true
                   }
            ];

            Atlas.ISBrowser.superclass.initComponent.apply(this, arguments);

            var srv  = this.getComponent('servers');
            var obj  = this.getComponent('objects');
            var data = this.getComponent('data');

            srv.on('serverChanged', obj.serverChanged, obj);
            // srv.getStore().load();
            obj.on('objectChanged', data.objectChanged, data);

            if(this.initialConfig.partition) { 
                this.setPartition(this.initialConfig.partition); 
            };
        }
});
Ext.reg('atlas-is-browser', Atlas.ISBrowser);

Atlas.OHBrowser = Ext.extend(Ext.Panel, {

        setPartition: function(partition_name) {
            this.items.get(0).partitionChanged(partition_name, Atlas.baseUrl + '/' + partition_name);
        },

        initComponent: function() {

                this.layout = 'border';
                this.items  = [ 
                        { xtype: 'atlas-provider-panel',  itemId: 'providers', region: 'west' , url: this.initialConfig.url, split: true, changer: this.initialConfig.changer, collapsible: true },
                        { xtype: 'atlas-histogram-panel',  itemId: 'histograms', region: 'center' },
                        { 
                          xtype: 'panel',
                          itemId: 'display', 
                          title: 'Histogram', 
                          region: 'east', 
                          split: true, 
                          width: 700,
                          height: 450,
                          layout: 'border',
                          items: [ { xtype: 'panel', region: 'center', items: { xtype: 'atlas-histogram-display', height: 450 } },
                                   { xtype: 'atlas-histogram-control', itemId: 'control', region: 'south',split: true, height: 450  } ]
                        }
                ];

                Atlas.OHBrowser.superclass.initComponent.apply(this, arguments);

                var prov    = this.getComponent('providers');
                var hists   = this.getComponent('histograms');
                var display = this.getComponent('display');
                var control = display.getComponent('control');

                prov.on('providerChanged', hists.providerChanged, hists);
                // prov.getStore().load();
                hists.on('histogramChanged', display.items.get(0).items.get(0).histogramChanged, display.items.get(0).items.get(0));
                control.display = display.items.get(0).items.get(0);

                if(this.initialConfig.partition) { this.setPartition(this.initialConfig.partition); }
        }
});
Ext.reg('atlas-oh-browser', Atlas.OHBrowser);

Atlas.OHTreeBrowser = Ext.extend(Ext.Panel,{
        setPartition: function(partition_name) {
            this.getComponent('providers').partitionChanged(partition_name, Atlas.baseUrl + '/' + partition_name);
        },

        constructor: function(config) {
            // set default type for histogram tree panel
            config = Ext.apply({
                htreeXtype: 'atlas-histogram-tree-panel'
            }, config);

            Atlas.OHTreeBrowser.superclass.constructor.call(this, config);
        },

        initComponent: function() {
                this.layout = 'border';
                this.items =  [ 
                        { 
                                xtype: this.htreeXtype,
                                itemId: 'providers', 
                                region: 'center' , 
                                url: this.initialConfig.url, 
                                split: true, 
                                changer: this.initialConfig.changer, 
                                collapsible: true 
                        },
                        { 
                                xtype: 'panel', 
                                title: 'Histogram', 
                                itemId: 'display', 
                                region: 'east', 
                                split: true, 
                                width: 700 ,
                                layout: 'border',
                                items: [ { xtype: 'panel', region: 'center', items: { xtype: 'atlas-histogram-display', height: 450 } },
                                         { xtype: 'atlas-histogram-control', itemId: 'control', region: 'south',split: true, height: 350  } ]
                        }
                ];
                Atlas.OHTreeBrowser.superclass.initComponent.apply(this, arguments);

                var tree = this.getComponent('providers');
                var display = this.getComponent('display');
                var control = display.getComponent('control');

                tree.on('histogramChanged', display.items.get(0).items.get(0).histogramChanged, display.items.get(0).items.get(0));
                control.display = display.items.get(0).items.get(0);

                if(this.initialConfig.partition) this.setPartition(this.initialConfig.partition);
                
        }
});
Ext.reg('atlas-oh-tree-browser', Atlas.OHTreeBrowser);

Atlas.OKSBrowser = Ext.extend(Ext.Panel, {
        setPartition: function(partition_name) {
                this.getComponent('classes').partitionChanged(partition_name, Atlas.baseUrl + '/' + partition_name);                
        },
        initComponent: function() {
                this.layout = 'border';
                this.items =  [ 
                        { xtype: 'atlas-oks-class-panel',  itemId: 'classes', region: 'west' , url: this.initialConfig.url, split: true, changer: this.initialConfig.changer, collapsible: true },
                        { xtype: 'atlas-oks-object-panel',  itemId: 'objects', region: 'center' },
                        { xtype: 'atlas-oks-data-panel',  itemId: 'attributes', region: 'east', split: true }
                ];
                Atlas.OKSBrowser.superclass.initComponent.apply(this, arguments);
                var cl = this.getComponent('classes');
                var obj = this.getComponent('objects');
                var attr = this.getComponent('attributes');
                cl.on('classChanged', obj.classChanged, obj);
                // cl.getStore().load();
                obj.on('objectChanged', attr.objectChanged, attr);

                if(this.initialConfig.partition) this.setPartition(this.initialConfig.partition);
        }
});
Ext.reg('atlas-oks-browser', Atlas.OKSBrowser);

Atlas.PMGBrowser = Ext.extend(Ext.Panel, {

        setPartition: function(partition_name) {
            this.getComponent('hosts').partitionChanged(partition_name, Atlas.baseUrl + '/' + partition_name);
        },

        initComponent: function() {
                this.layout = 'border';
                this.items = [ 
                        { xtype: 'atlas-pmg-host-panel', itemId: 'hosts',  columnWidth: 0.25, region: 'west' , url: this.initialConfig.url, split: true, changer: this.initialConfig.changer, collapsible: true },
                        { xtype: 'atlas-pmg-process-panel',  itemId: 'processes', columnWidth: 0.5, region: 'center' },
                        { xtype: 'atlas-pmg-data-panel',  itemId: 'data', columnWidth: 0.25, region: 'east', split: true }
                ];

                Atlas.PMGBrowser.superclass.initComponent.apply(this, arguments);

                var hosts = this.getComponent('hosts');
                var proc  = this.getComponent('processes');
                var data  = this.getComponent('data');
                hosts.on('hostChanged', proc.hostChanged, proc);
                proc.on('objectChanged', data.objectChanged, data);

                if(this.initialConfig.partition) this.setPartition(this.initialConfig.partition);
        }
});
Ext.reg('atlas-pmg-browser', Atlas.PMGBrowser);
