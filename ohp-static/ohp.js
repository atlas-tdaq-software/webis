
var smallScreen = null;
var allowLoading = null;
var intervalId = null;

// Event binding for startup of the page
$(document).on("mobileinit", init_jquerymobile);
$(document).ready(ready_jquerymobile);


// Setting of options and events that can be set before loading the DOM
function init_jquerymobile()
{
    // Allow transitions to the same page
    $.mobile.changePage.defaults.allowSamePageTransition = true;
    $.mobile.defaultPageTransition ='none';
}


// Setting of events and settings that requiere the DOM to be ready
function ready_jquerymobile()
{
    // Setup the panel (menu)
    $( "body>[data-role='panel']" ).panel().enhanceWithin();
    // Fixed external header and footer
    $( "[data-role='header'], [data-role='footer']" ).toolbar();
    // External popup
    $("#graph-popup").popup().enhanceWithin();

    // Remove active state from popup buttons
    $(document).on("click", function(event){ $(".popup-btn").removeClass($.mobile.activeBtnClass);});
    
    // This should be here but in test page without Atlas framework this produces an error and interrupts the loading
    //webis.init();
    // Display the status of the partition in the external header
    //var partition = $("#curpartition").attr("data-ohp-partition");
    //get_status(partition);
}


// Onload function for jsroot, settings that need JsRoot
function start_jsroot()
{
    // Functionalities depending on screen size, binding popup-btn
    check_size();
    $(window).resize(check_resize);
    
    // Get the (default) configs from startpage
    set_configs();

    // Initialize the graphs on the first page loaded or reloaded
    var activePage = $.mobile.pageContainer.pagecontainer("getActivePage")[0];
    if (activePage.id == "page_Rates" || activePage.id == "page_WTRP" || activePage.id == "page_Browser"){
        display_plot_iframe(activePage);
    } 
    else {
        display_plot_divs(activePage);
    }
    
    // Events and bindings which refere at some point to JsRoot, so savely initialise them here    
    
    // Event on page transition to load in the graphs
    $(document).on("pagecontainerchange", change_page_event);

    // Events for opening and closing the popup
    $(document).on("popupbeforeposition", popup_open);
    $(document).on("popupafterclose", popup_close);
    // Popup button functionality next and prev
    $(".popup-next-btn").on("click", popup_next);
    $(".popup-prev-btn").on("click", popup_prev);

    // Refresh graphs button on click functionality
    $(".refresh-button").on("click", refresh_plots);

    $("body").on("click", "a.auto-up-btn[data-ohp-func='start']", start_automatic_update);
    $("body").on("click", "a.auto-up-btn[data-ohp-func='stop']", stop_automatic_update);

    // Load all button for small screens
    $(".load-all-button").on("click", load_all_plots);

    // Graph button
    $("body").on("click", "a.popup-btn[data-ohp-func='popup']", graph_button_popup);
    $("body").on("click", "a.popup-btn[data-ohp-func='load']", graph_button_load);
    $("body").on("click", "a.popup-btn[data-ohp-func='delete']", graph_button_delete);
    
    // Options submit button
    $(".submit-set-btn").on("click", set_configs);

    // Register the JSROOT graphs for Browser resize, do the plots need to exist?
    [].forEach.call(document.getElementsByClassName("plot"), function(graphdiv){JSROOT.RegisterForResize(escape_idstring(graphdiv.id));});
    
    webis.init();
    // Display the status of the partition in the external header
    var partition = $("#curpartition").attr("data-ohp-partiton");
    get_status(partition);
}


function check_size()
{
    if ($(".menu-button").css("display")=="none"){
        smallScreen = false;
        $(".plot").addClass("display-plot");
    }
    else {
        smallScreen = true;
        $("#label-timedelay").html("s");
        $(".auto-up-btn").addClass("ui-btn-icon-notext ui-corner-all");
        $(".refresh-button").addClass("ui-btn-icon-notext ui-corner-all");
        // Add classes to graphs and buttons, on start of page allowLoading=='null' but handled in initial set_configs(), then used here?
        if (allowLoading){
            $(".plot").addClass("display-plot");
            $(".popup-btn").removeClass("ui-icon-eye").addClass("ui-icon-delete")
                .attr("data-ohp-func", "delete");
        }
        else {
            $(".popup-btn").attr("data-ohp-func", "load");
        }
    }
}


function check_resize()
{
    if ($(".menu-button").css("display")=="none" && smallScreen){
        smallScreen = false;
        // Remove delete icons in buttons
        $(".popup-btn.ui-icon-delete").removeClass("ui-icon-delete").addClass("ui-icon-eye");
        $(".popup-btn").attr("data-ohp-func", "popup");
        $("#label-timedelay").html("time [s]");
        $(".auto-up-btn").removeClass("ui-btn-icon-notext ui-corner-all");
        $(".refresh-button").removeClass("ui-btn-icon-notext ui-corner-all");
        // All graphs get displayed
        $(".plot").addClass("display-plot");
        // Load all plots on active page
        refresh_plots();        
    }
    else if ($(".menu-button").css("display")!="none" && !smallScreen){
        smallScreen = true;
        $("#label-timedelay").html("s");
        $(".auto-up-btn").addClass("ui-btn-icon-notext ui-corner-all");
        $(".refresh-button").addClass("ui-btn-icon-notext ui-corner-all");
        // Remove all plots
        if (!allowLoading){
            var activePage = $.mobile.pageContainer.pagecontainer("getActivePage");
            delete_plot_divs(activePage[0]);
            $(".plot").removeClass("display-plot");
            $(".popup-btn").attr("data-ohp-func", "load");
        }
        else {
            $(".popup-btn").removeClass("ui-icon-eye").addClass("ui-icon-delete")
                .attr("data-ohp-func", "delete");
        }
        // Check for open popup
        if ($("#graph-popup-drawing").hasClass("display-plot")){
            var graphId = $("#graph-popup-drawing").attr("data-ohp-graph-id");
            $("#" + escape_idstring(graphId)).addClass("display-plot");
            $("#graph-popup").popup("close");
            $("#" + escape_idstring(graphId) + "_btn").removeClass("ui-icon-eye").addClass("ui-icon-delete")
                .attr("data-ohp-func", "delete");
        }
    }
}


function display_plot_divs(pageElement)
{
    [].forEach.call(pageElement.getElementsByClassName("display-plot"), function(graphdiv){
        request_graph(graphdiv);
    });
}


function display_plot_iframe(pageElement)
{
    var iframe = pageElement.getElementsByClassName("ext-html");
    iframe[0].src = iframe[0].title;
}


function delete_plot_divs(pageElement)
{
    [].forEach.call(pageElement.getElementsByClassName("plot"), function(graphdiv){
        JSROOT.cleanup(escape_idstring(graphdiv.id));
    });
}


function delete_plot_iframe(pageElement)
{
    var iframe = pageElement.getElementsByClassName("ext-html")
    iframe[0].src = "";
    iframe[0].innerHTML = "";
}


function request_graph(graphdiv)
{
    var req=JSROOT.NewHttpRequest(graphdiv.getAttribute("data-ohp-graph-address"), 'object', function(obj){
        JSROOT.redraw(escape_idstring(graphdiv.id), obj, "hist");
    });
    req.send(null);
}


function change_page_event(event, ui)
{
    var pageBefore = ui.prevPage;
    if (pageBefore){
        if (pageBefore[0].id == "page_Rates" || pageBefore[0].id == "page_WTRP" || pageBefore[0].id == "page_Browser"){
            delete_plot_iframe(pageBefore[0]);
        } else {
            delete_plot_divs(pageBefore[0]);
            if (smallScreen && !allowLoading){
                $(".popup-btn.ui-icon-delete").removeClass("ui-icon-delete").addClass("ui-icon-eye")
                    .attr("data-ohp-func", "load");
                $(".display-plot").removeClass("display-plot");
            }
        }
    }
    var pageAfter = ui.toPage;
    if (pageAfter[0].id == "page_Rates" || pageAfter[0].id == "page_WTRP" || pageAfter[0].id == "page_Browser"){
        display_plot_iframe(pageAfter[0]);
    } else {
        display_plot_divs(pageAfter[0]);
    }
}


function refresh_plots(event)
{
    if (event!=null){
        event.preventDefault();
    }
    // Check if popup is open 
    if ($("#graph-popup-drawing").hasClass("display-plot")){
        var graphdivpopup = document.getElementById("graph-popup-drawing");
        JSROOT.cleanup(escape_idstring(graphdivpopup.id));
        request_graph(graphdivpopup);
    }
    else {
        var activePage = $.mobile.pageContainer.pagecontainer("getActivePage");
        [].forEach.call(activePage[0].getElementsByClassName("display-plot"), function(graphdiv){
            JSROOT.cleanup(escape_idstring(graphdiv.id));
            request_graph(graphdiv);
        });
    }
}


function load_all_plots(event)
{
    event.preventDefault();
    $(".plot", $.mobile.pageContainer.pagecontainer("getActivePage")).addClass("display-plot");
    $(".popup-btn", $.mobile.pageContainer.pagecontainer("getActivePage")).removeClass("ui-icon-eye").addClass("ui-icon-delete")
        .attr("data-ohp-func", "delete");
    refresh_plots();
}


function start_automatic_update()
{
    $(".auto-up-btn").removeClass("ui-icon-recycle").addClass("ui-icon-delete")
        .attr("data-ohp-func", "stop")
        .html("Stop refreshing");
    $("#timedelay").textinput('disable');
    var timeInterval = $(".input-timedelay").val();  //get timevalue
    timeInterval = 1000*timeInterval;
    intervalId = setInterval(refresh_plots, timeInterval);
}

function stop_automatic_update()
{
    $(".auto-up-btn").removeClass("ui-icon-delete").addClass("ui-icon-recycle")
        .attr("data-ohp-func", "start")
        .html("Auto refresh");
    //get intervalId, now global
    clearInterval(intervalId);
    $("#timedelay").textinput("enable");
}


function set_configs()
{
    // Zooming
    JSROOT.gStyle.ZoomWheel = ($("#flip-zooming").val() == "off") ? false:true;
    // Loading of graphs on small screens
    allowLoading = ($("#flip-allow-loading").val() == "off") ? false:true;
    if (allowLoading && smallScreen != 'null' && smallScreen){
        $(".plot").addClass("display-plot");
        $(".popup-btn").removeClass("ui-icon-eye").addClass("ui-icon-delete")
            .attr("data-ohp-func", "delete");
    }
    else if (!allowLoading && smallScreen != 'null' && smallScreen){
        $(".plot").removeClass("display-plot");
        $(".popup-btn").removeClass("ui-icon-delete").addClass("ui-icon-eye")
            .attr("data-ohp-func", "load");
    }
    // Colour palette
    JSROOT.gStyle.Palette = parseInt($("#input-palette").val());
}


function popup_open(event)
{
    request_graph($("#graph-popup-drawing")[0]);
}


function popup_close(event)
{
    $("#graph-popup-drawing").removeClass("display-plot")
        .attr({"data-ohp-graph-address": "", 
               "data-ohp-graph-id": "", 
               "data-ohp-graph-number": 0, 
               "data-ohp-graph-number-min": 1, 
               "data-ohp-graph-number-max": 1});
    JSROOT.cleanup("graph-popup-drawing");
    refresh_plots();
}


function graph_button_load(event)
{
    event.preventDefault();
    $(event.target).removeClass("ui-icon-eye").addClass("ui-icon-delete")
        .attr("data-ohp-func", "delete");
    var graphId = escape_idstring($(event.target).attr("data-ohp-graph-id"));
    $("#" + graphId).addClass("display-plot");
    request_graph($("#" + graphId)[0]);
}


function graph_button_delete(event)
{
    event.preventDefault();
    $(event.target).removeClass("ui-icon-delete").addClass("ui-icon-eye")
        .attr("data-ohp-func", "load");
    var graphId = escape_idstring($(event.target).attr("data-ohp-graph-id"));
    $("#" + graphId).removeClass("display-plot");
    JSROOT.cleanup(graphId);
}


function graph_button_popup(event)
{
    event.preventDefault();
    var graphId = $(event.target).attr("data-ohp-graph-id");
    var graphAddress = $("#" + escape_idstring(graphId)).attr("data-ohp-graph-address");
    var graphNumber = $("#" + escape_idstring(graphId)).attr("data-ohp-graph-number");

    var activePage = $.mobile.pageContainer.pagecontainer("getActivePage");
    var graphNumberMin = activePage.attr("data-ohp-graph-number-min");
    var graphNumberMax = activePage.attr("data-ohp-graph-number-max");

    $("#graph-popup-drawing").attr({"data-ohp-graph-address": graphAddress,
                                    "data-ohp-graph-id": graphId,
                                    "data-ohp-graph-number": graphNumber, 
                                    "data-ohp-graph-number-min": graphNumberMin,
                                    "data-ohp-graph-number-max": graphNumberMax})
        .addClass("display-plot");
    
    (graphNumber == graphNumberMax) ? $("#popup-next").addClass("ui-disabled"):$("#popup-next").removeClass("ui-disabled");
    
    (graphNumber == graphNumberMin) ? $("#popup-prev").addClass("ui-disabled"):$("#popup-prev").removeClass("ui-disabled");

    $("#graph-popup").popup("open");
}


function popup_next(event)
{
    event.preventDefault();
    var activePageId = $.mobile.pageContainer.pagecontainer("getActivePage").attr("id");
    var graphNumber = parseInt($("#graph-popup-drawing").attr("data-ohp-graph-number"))+1;
    var graphId = activePageId.substring(5) + "_" + graphNumber;
    var graphAddress = $("#" + escape_idstring(graphId)).attr("data-ohp-graph-address");
    
    $("#popup-prev").removeClass("ui-disabled");
    $("#graph-popup-drawing").attr({"data-ohp-graph-address": graphAddress, 
                                    "data-ohp-graph-id": graphId, 
                                    "data-ohp-graph-number": graphNumber});
    JSROOT.cleanup("graph-popup-drawing");
    request_graph($("#graph-popup-drawing")[0]);
    
    if (graphNumber == $("#graph-popup-drawing").attr("data-ohp-graph-number-max")){
        $("#popup-next").addClass("ui-disabled");
    }
}


function popup_prev(event)
{
    event.preventDefault();
    var activePageId = $.mobile.pageContainer.pagecontainer("getActivePage").attr("id");
    var graphNumber = parseInt($("#graph-popup-drawing").attr("data-ohp-graph-number"))-1;
    var graphId = activePageId.substring(5) + "_" + graphNumber;
    var graphAddress = $("#" + escape_idstring(graphId)).attr("data-ohp-graph-address");

    $("#popup-next").removeClass("ui-disabled")
    $("#graph-popup-drawing").attr({"data-ohp-graph-address": graphAddress, 
                                    "data-ohp-graph-id": graphId, 
                                    "data-ohp-graph-number": graphNumber});
    JSROOT.cleanup("graph-popup-drawing");
    request_graph($("#graph-popup-drawing")[0]);

    if (graphNumber == $("#graph-popup-drawing").attr("data-ohp-graph-number-min")){
        $("#popup-prev").addClass("ui-disabled");
    }
}


// In ids for jquery selectors escape special characters with \
function escape_idstring(inputid)
{
    return inputid.replace(/([ {}\|`\^@\?%#;&,.+*~\':"!^$[\]()=>|\/])/g,'\\$1');
}


var root_url = '/info/current/';
var base_url = root_url + 'ATLAS'

function show_state(response)
{
    var s = webis.get_attribute(response.responseXML, 'state') || 'ABSENT';
    document.getElementById('status').innerHTML = s;
    document.getElementById('status').className = s;
}

function show_error(response)
{
  document.getElementById('status').innerHTML = 'ABSENT';
  document.getElementById('status').className = 'ABSENT';
}

function get_status(partition)
{
    partition = partition || 'ATLAS';
    webis.request(root_url + partition + '/is/RunCtrl/RunCtrl.RootController', { type: 'RCStateInfo'}, show_state, show_error);
    setTimeout(get_status, 60000);
}

