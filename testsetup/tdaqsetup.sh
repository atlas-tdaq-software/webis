#!/bin/bash

#Connect to pc-tbed-pub via ssh
alias tbedssh='ssh pc-tbed-pub'

#Path variables, fill in your specific pathes (absolute pathes!)
pathToTdaq='/afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh'
pathToWebis='.../webis'
pathToWebisServer='.../webis_server/webis2'

#Setup for tdaq code
tdaqVersion='tdaq-06-01-01 x86_64-slc6-gcc49-opt'
#alias setupTdaq='source $pathToTdaq $tdaqVersion'
function setupTdaq (){
	export ROOT_INCLUDE_PATH=${pathToWebisServer}/include:${ROOT_INCLUDE_PATH}
	source ${pathToTdaq} ${tdaqVersion}
	export LD_LIBRARY_PATH=${pathToWebisServer}/lib:${LD_LIBRARY_PATH}
}

#Setup for starting a webis server
alias webisServerSetup='${pathToWebisServer}/server.py --port=5000 --www=$pathToWebis &'

#Execute oph2webis.py with argument of the name of the outputfile
function ohp2webispython(){
	startlocation=${pwd}
	cd ${pathToWebis}/testsetup/
	python ohp2webis.py ${pathToWebis}/testsetup/test.ohp.xml ${1:-../ohp/MytestJsRoot}
	cd ${startlocation}
}
