#!/usr/bin/env tdaq_python
import sys
from ipc import IPCPartition
import oh
import ROOT


partition = IPCPartition(len(sys.argv) > 1 and sys.argv[1] or 'mytestJs')

provider = oh.OHRootProvider(partition, 'Histogramming', 'Test', None)

for i in range(16):
  h = ROOT.TH1F('MyTest-%i' % i, 'MyTest-%i' % i,100, 0.0, 100.0)
  h1 = ROOT.TH1F('MyTestL-%i' % i, 'MyTestL-%i' % i,100, 0.0, 100.0)
  h.Fill(10, 5)
  h.Fill(20, 10)

  h1.Fill(5, 10)
  h1.Fill(30, 15)

  provider.publish(h,'MyTest-%i' %i)
  provider.publish(h1,'MyTestL-%i' %i)

for i in xrange(4):
  h2 = ROOT.TH2D('MyTest2d-%i' % i, 'MyTest2d-%i' % i, 20, 0.0, 20.0, 20, 0.0, 20)
  
  for j in xrange(20):
    for k in xrange(20):
      bin = h2.GetBin(j+1, k+1)
      val = 0
      if i % 4 == 0:
        val = j + k
      elif i % 4 == 1:
        val = j + 19 - k
      elif i % 4 == 2:
        val = 38 - j - k
      else:
        val = 19 - j + k

      h2.SetBinContent(bin, val)
  h2.GetXaxis().SetTitle('Xaxis #muX_{x}')
  h2.GetYaxis().SetTitle('Yaxis #alpha^{#nu}')
  provider.publish(h2, 'MyTest2d-%i' %i)

for i in xrange(4):
  g1 = ROOT.TGraph(50)
  
  for j in xrange(50):
    x = 0.1*j
    y = 0.1*j*(i+1)
   # yerr = 0.01*j
    g1.SetPoint(j, x, y)
   # g1.SetPointError(j, 0, yerr)
  g1.SetName('MyTestGraph-%i' %i)
  g1.SetTitle('MyTestGraph-%i' % i)
  g1.GetXaxis().SetTitle('x')
  g1.GetYaxis().SetTitle('y')
  provider.publish(g1, 'MyTestGraph-%i' %i)
  
for i in xrange(2):  
  hl = ROOT.TH1F('LayerItem-%i' % i, 'LayerItem-%i' % i,100, 0.0, 100.0)
  hl.Fill(10, 5)
  hl.Fill(20, 10)

  provider.publish(hl,'LayerItem-%i' %i)
