<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="domain">
  <script type="text/javascript">
     <xsl:text>
       try {
         document.domain = 'cern.ch'
       } catch(e)
       {}

       function get_text(el)
       {
           if(el.nodeType == 3) return el.nodeValue;
           if(!el.childNodes) return '';
           var result = '';
           for(var i = 0; i &lt; el.childNodes.length; i++) {
               result += get_text(el.childNodes[i]);
           }
           return result;
       }

       function search_rows(row, expr) 
       {
           var regex = new RegExp(expr, 'i');
           var rows = document.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
           for(var i = 0; i &lt; rows.length; i++) {
                  if(regex == '') {
                      rows[i].style.display = 'table-row';
                  } else {
                      var text = row == 0 ? get_text(rows[i].getElementsByTagName('th')[row]) :  get_text(rows[i].getElementsByTagName('td')[row-1]);
                      if(text.search(regex) == -1) {
                        rows[i].style.display = 'none';
                      } else {
                        rows[i].style.display = 'table-row';
                      }
                  }
              }
              return false;
       }

       function do_nothing(e)
       {
	   if (!e) var e = window.event
           alert(window.event);
	   e.cancelBubble = true;
	   if (e.stopPropagation) e.stopPropagation();
           return false;
       }
     </xsl:text>
  </script>
  <script type="text/javascript" src="/tdaq/web_is/lib/sorttable.js"></script>
</xsl:template>

<xsl:template name="style">
   <style type="text/css">
     <xsl:text>
     th, td { font-size: 10pt; }
     th { text-align: left; }
     td, thead th { text-align: center; }
     table.sortable thead {
        background-color: #ddd;
        color:#666666;
        font-weight: bold;
        cursor: pointer;
     }
     table { margin-left: 5%; width: 90% }
     table a { cursor: pointer; text-decoration: none; color: black; }
     table a:focus { color: blue; }
     table a:hover { color: blue; }
     tbody tr:nth-child(2n) { background-color: #eee; }
     h1 { border: solid;
       border-width: 1px 2px 2px 1px;
       background-color: #ddd;
       text-align: center;
       width: 90%; margin-left: 5%; 
     }
     h2 { border: solid;
       border-width: 1px 2px 2px 1px;
       border-color: #000;
       background-color: #fff;
       color: #f00;
       text-align: center;
       width: 80%; margin-left: 10%; 
     }
     form { display: inline; }
     </xsl:text>
   </style>
</xsl:template>

<xsl:template name="ConvertDecToHex">
  <xsl:param name="index" />
  <xsl:if test="$index > 0">
    <xsl:call-template name="ConvertDecToHex">
      <xsl:with-param name="index" select="floor($index div 16)" />
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="$index mod 16 &lt; 10">
        <xsl:value-of select="$index mod 16" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$index mod 16 = 10">a</xsl:when>
          <xsl:when test="$index mod 16 = 11">b</xsl:when>
          <xsl:when test="$index mod 16 = 12">c</xsl:when>
          <xsl:when test="$index mod 16 = 13">d</xsl:when>
          <xsl:when test="$index mod 16 = 14">e</xsl:when>
          <xsl:when test="$index mod 16 = 15">f</xsl:when>
          <xsl:otherwise>A</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:if>
</xsl:template>

<!-- RELEASES -->

<xsl:template match="/releases">
<html> 
  <head>
  <title>Releases</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1>Releases</h1>
    <table rules="all" frame="border" class="sortable">
    <thead><tr><th>Name</th></tr></thead><tbody>
    <xsl:for-each select="releases/release">
        <tr><th><a><xsl:attribute name="href"><xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/></a> </th></tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- PARTITIONS -->

<xsl:template match="/partitions">
<html> 
  <head>
  <title>Partition List</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>

  <body>
    <h1>Partitions</h1>
    <xsl:if test="@error"><h2>Error: <xsl:value-of select="/partitions/@error"/></h2></xsl:if>
    <table rules="all" frame="border" class="sortable">

    <thead>
       <tr>
         <th>Partition <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Partition" type="text" autofocus="autofocus"/></form></th>
         <th class="sorttable_nosort" colspan="5">Service</th>
       </tr>
    </thead>
    <tbody>
      <xsl:for-each select="partition">
        <tr>
           <th ><xsl:value-of select="@name"/></th>
           <td style="text-align: center"><a><xsl:attribute name="href"><xsl:value-of select="/partitions/@release"/>/<xsl:value-of select="@name"/>/is</xsl:attribute>Information Service</a></td>
           <td style="text-align: center"><a><xsl:attribute name="href"><xsl:value-of select="/partitions/@release"/>/<xsl:value-of select="@name"/>/oh</xsl:attribute>Histograms</a></td>
           <td style="text-align: center"><a><xsl:attribute name="href"><xsl:value-of select="/partitions/@release"/>/<xsl:value-of select="@name"/>/oks</xsl:attribute>OKS</a></td>
           <td style="text-align: center"><a><xsl:attribute name="href"><xsl:value-of select="/partitions/@release"/>/<xsl:value-of select="@name"/>/emon</xsl:attribute>EMON Samplers</a></td>
           <td style="text-align: center"><a><xsl:attribute name="href"><xsl:value-of select="/partitions/@release"/>/<xsl:value-of select="@name"/>/emon_monitor</xsl:attribute>EMON Monitors</a></td>
         </tr>
      </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- SERVICES -->

<xsl:template match="/services">
<html> 
  <head>
  <title>Services</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1><xsl:value-of select="/services/@partition"/></h1>
    <xsl:if test="@error"><h2>Error: <xsl:value-of select="/services/@error"/></h2></xsl:if>
    <table rules="all" frame="border" class="sortable">
    <thead><tr><th>Service</th></tr></thead><tbody>
    <xsl:for-each select="service">
        <tr>
            <th><a><xsl:attribute name="href"><xsl:value-of select="/services/@partition"/>/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@descr"/></a></th>
        </tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>


<!-- SERVERS -->

<xsl:template match="/servers">
<html> 
  <head>
  <title>IS Servers</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1><xsl:value-of select="/servers/@partition"/></h1>
    <xsl:if test="@error"><h2>Error: <xsl:value-of select="/servers/@error"/></h2></xsl:if>
    <table rules="all" frame="border" class="sortable">
    <thead><tr><th>Server <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th><th>User</th><th>Host</th><th>Time</th><th>PID</th></tr></thead><tbody>
    <xsl:for-each select="server">
        <tr>
            <th><a><xsl:attribute name="href">is/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/></a></th>
            <td><xsl:value-of select="@owner"/></td>
            <td><xsl:value-of select="@host"/></td>
            <td><xsl:value-of select="@time"/></td>
            <td><xsl:value-of select="@pid"/></td>
        </tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- PROVIDERS -->

<xsl:template match="/providers">
<html> 
  <head>
  <title>OH Providers</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1><xsl:value-of select="/providers/@partition"/></h1>
    <xsl:if test="@error"><h2>Error: <xsl:value-of select="/providers/@error"/></h2></xsl:if>
    <table rules="all" frame="border" class="sortable">
    <thead><tr><th>Provider <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th></tr></thead><tbody>
    <xsl:for-each select="provider">
        <tr><th><a><xsl:attribute name="href">is/<xsl:value-of select="@server"/>?regex=<xsl:value-of select="@name"/>\..*</xsl:attribute><xsl:value-of select="@name"/></a> </th></tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- OKS CLASSES -->

<xsl:template match="/oks">
<html> 
  <head>
  <title>OKS Classes</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1><xsl:value-of select="/oks/@partition"/></h1>
    <table rules="all" frame="border" class="sortable">
    <thead><tr><th>Class <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th></tr></thead><tbody>
    <xsl:for-each select="oks-class">
        <tr><th><a><xsl:attribute name="href">oks/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/></a></th></tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- IS NAMES -->

<xsl:template match="/names">
<html> 
  <head>
  <title>IS Objects</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1><xsl:value-of select="/names/@server"/></h1>
    <table rules="all" frame="border" class="sortable">
    <thead><tr><th>Object <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
               <th>Type <form onsubmit="return search_rows(1, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
               <th>Time</th>
           </tr>
    </thead><tbody>
    <xsl:for-each select="obj">
        <tr>
          <xsl:choose>
             <xsl:when test="substring(@type,0,14) = 'HistogramData'">
                 <th><a><xsl:attribute name="href">../oh/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/> </a> [<a><xsl:attribute name="href">../is/<xsl:value-of select="/names/@server"/>/<xsl:value-of select="@name"/></xsl:attribute>Raw</a>] [<a><xsl:attribute name="href">/tdaq/web_is/utils/histo_js.html?histo=/info/current/<xsl:value-of select="/names/@partition"/>/oh/<xsl:value-of select="@name"/>?type=json</xsl:attribute>JSON</a>] [<a><xsl:attribute name="href">../oh/<xsl:value-of select="@name"/>?type=root</xsl:attribute>Root</a>]</th><td><xsl:value-of select="@type"/></td>
            </xsl:when>
             <xsl:when test="substring(@type,0,12) = 'ProfileData'">
                 <th><a><xsl:attribute name="href">../oh/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/> </a> [<a><xsl:attribute name="href">../is/<xsl:value-of select="/names/@server"/>/<xsl:value-of select="@name"/></xsl:attribute>Raw</a>]</th><td><xsl:value-of select="@type"/></td>
            </xsl:when>
             <xsl:when test="substring(@type,0,6) = 'Graph'">
                 <th><a><xsl:attribute name="href">../oh/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/> </a> [<a><xsl:attribute name="href">../is/<xsl:value-of select="/names/@server"/>/<xsl:value-of select="@name"/></xsl:attribute>Raw</a>]</th><td><xsl:value-of select="@type"/></td>
            </xsl:when>
            <xsl:otherwise>
                 <th><a><xsl:attribute name="href"><xsl:value-of select="/names/@server"/>/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/></a> </th><td><xsl:value-of select="@type"/></td>
            </xsl:otherwise>
          </xsl:choose>
          <td><xsl:value-of select="@time"/></td>
          </tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- IS OBJECT -->

<xsl:template match="/objects">

<html> 
  <head>

  <title>IS Object</title>

  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>

  <body>
     <xsl:apply-templates select="obj"/>
  </body>

</html>

</xsl:template>

<xsl:template match="obj">
   <p/>
   <h1><xsl:value-of select="@name"/> ( <xsl:value-of select="@time"/> )</h1>

   <table rules="all" frame="border" class="sortable">
      <thead>
         <tr>
            <th>Attribute</th>
            <th>Value</th>
         </tr>
      </thead>
      <tbody>
        <xsl:apply-templates select="attr" mode="is"/>
      </tbody>
   </table>
</xsl:template>

<xsl:template match="attr" mode="is">
   <tr>
      <xsl:attribute name="title">
         <xsl:value-of select="@descr"/>
      </xsl:attribute>

      <th>
          <xsl:value-of select="@name"/>
      </th>

      <td>
          <xsl:if test="@multi"><xsl:text>[ </xsl:text></xsl:if>
          <xsl:apply-templates select="v"/>
          <xsl:if test="@multi"><xsl:text> ]</xsl:text></xsl:if>
      </td>

   </tr>
</xsl:template>

<xsl:template match="attr[@multi='1']/v">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
       <xsl:text>, </xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="attr[@fmt='h']/v">
  <xsl:text>0x</xsl:text>
  <xsl:call-template name="ConvertDecToHex">
    <xsl:with-param name="index"><xsl:value-of select="."/></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="v">
    <xsl:value-of select="."/>
</xsl:template>

<!-- OKS names -->

<xsl:template match="/oks-objects">
<html> 
  <head>
  <title>OKS Objects</title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1><xsl:value-of select="/oks-objects/@type"/></h1>
    <table rules="all" frame="border" class="sortable">

    <thead><tr><th>Object <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
               <th>Type <form onsubmit="return search_rows(1, this.elements[0].value)"> <input onclick="event.cancelBubble = true;"  size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
           </tr>
    </thead><tbody>
    <xsl:for-each select="obj">
        <tr>
            <th><a><xsl:attribute name="href"><xsl:value-of select="@type"/>/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/></a></th>
            <td><a><xsl:attribute name="href"><xsl:value-of select="@type"/>/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@type"/></a></td>
        </tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- OKS Data -->

<xsl:template match="/oks-data">
<html> 
  <head>
  <title>OKS Object</title>

  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>

  </head>
  <body>
    <p/>
    <xsl:apply-templates select="obj" mode="oks"/>
  </body>
</html>
</xsl:template>

<xsl:template match="obj" mode="oks">
      <h1><xsl:value-of select="@name"/> : <xsl:value-of select="@type"/></h1>
      <table rules="all" frame="border" class="sortable">
         <thead><tr><th>Attribute</th><th>Type</th><th>Value</th></tr></thead>
         <tbody>
            <xsl:apply-templates select="attr" mode="oks"/>
            <xsl:apply-templates select="rel"/>
         </tbody>
      </table>
</xsl:template>

<xsl:template match="rel[@multi='False']">
  <tr>
    <th><xsl:value-of select="@name"/></th><td><xsl:value-of select="@type"/></td>
    <td>
         <xsl:apply-templates select="target"/>
    </td>
 </tr>
</xsl:template>

<xsl:template match="rel[@multi='True']">
  <tr>
    <th><xsl:value-of select="@name"/></th><td><xsl:value-of select="@type"/></td>
    <td><select>
         <xsl:apply-templates select="target"/>
        </select>
    </td>
 </tr>
</xsl:template>

<xsl:template match="attr" mode="oks">
  <tr>
  <th><xsl:value-of select="@name"/></th><td><xsl:value-of select="@type"/></td>
     <td>
      <xsl:apply-templates select="val"/>
     </td>
  </tr>
</xsl:template>

<xsl:template match="val">
       <span class="value">
         <xsl:value-of select="."/><xsl:text> </xsl:text>
        </span>
    <xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="rel[@multi='True']/target">
     <option>
        <xsl:attribute name="onclick">window.location.href="../<xsl:value-of select="@type"/>/<xsl:value-of select="@name"/>"</xsl:attribute>
        <xsl:value-of select="@name"/>@<xsl:value-of select="@type"/>
     </option>
</xsl:template>

<xsl:template match="rel[@multi='False']/target">
     <button>
        <xsl:attribute name="onclick">window.location.href="../<xsl:value-of select="@type"/>/<xsl:value-of select="@name"/>"</xsl:attribute>
        <xsl:value-of select="@name"/>@<xsl:value-of select="@type"/>
     </button>
</xsl:template>

<!-- EMON MONITORS -->

<xsl:template match="/monitors">
<html> 
  <head>
    <title>EMON Monitors for <xsl:value-of select="/monitors/@partition"/></title>
    <xsl:call-template name="domain"></xsl:call-template>
    <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1>EMON Monitors for <xsl:value-of select="/monitors/@partition"/></h1>
    <table rules="all" frame="border" class="sortable">
    <thead><tr>
      <th>Name <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
      <th>Update time</th>
      <th>Host</th>
      <th>Pid</th>
      <th>Active</th>
      <th>Sampler Type <form onsubmit="return search_rows(5, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
      <th>Sampler Names <form onsubmit="return search_rows(6, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
      <th>Selection Criteria <form onsubmit="return search_rows(7, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
      <th>Received Events</th>
      <th>Buffered Events</th>
      <th>Forwarded Events</th>
      <th>Dropped Events</th>
      <th>Child Monitors</th>
      <th>Dispersion</th>
      <th>Buffer Size</th>
      <th>Buffer Occupancy</th>
    </tr></thead>
    <tbody>
    <xsl:for-each select="monitor">
        <tr>
          <th><xsl:value-of select="@name"/></th>
          <td><xsl:value-of select="@time"/></td>
          <td><xsl:value-of select="@m_host"/></td>
          <td><xsl:value-of select="@m_pid"/></td>
          <td><xsl:value-of select="@m_active"/></td>
          <td><xsl:value-of select="@m_sampler_type"/></td>
          <td><textarea rows="1" readonly="readonly"><xsl:value-of select="@m_sampler_names"/></textarea></td>
          <td><xsl:value-of select="@m_selection_criteria"/></td>
          <td><xsl:value-of select="@m_received_events"/></td>
          <td><xsl:value-of select="@m_buffered_events"/></td>
          <td><xsl:value-of select="@m_forwarded_events"/></td>
          <td><xsl:value-of select="@m_dropped_events"/></td>
          <td><xsl:value-of select="@m_child_monitors"/></td>
          <td><xsl:value-of select="@m_dispersion"/></td>
          <td><xsl:value-of select="@m_buffer_size"/></td>
          <td><xsl:value-of select="@m_buffer_occupancy"/></td>
        </tr>
    </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<!-- EMON SAMPLERS -->

<xsl:template match="/samplers">
<html>
  <head>
    <title>EMON Samplers for <xsl:value-of select="@partition"/></title>
    <xsl:call-template name="domain"></xsl:call-template>
    <xsl:call-template name="style"></xsl:call-template>
  </head>
  <body>
    <h1>EMON Samplers for <xsl:value-of select="@partition"/></h1>
    <table rules="all" frame="border" class="sortable">
    <thead><tr>
      <th>Name <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
      <th>Type <form onsubmit="return search_rows(1, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Search" type="text" autofocus="autofocus"/></form></th>
      <th>Update Time</th>
      <th>Host</th>
      <th>Pid</th>
      <th>Active</th>
      <th>Active Channels</th>
      <th>Sampled Events</th>
      <th>Dropped Events</th>
      <th>Speedup Requests</th>
      <th>Slowdown Requests</th>
      <th>Maximum Channels</th>
    </tr> </thead>
    <tbody>
      <xsl:for-each select="sampler">
        <tr>
          <th>
            <xsl:value-of select="@m_name"/>
          </th>
          <td>
            <a><xsl:attribute name="href">emon/<xsl:value-of select="@m_type"/></xsl:attribute><xsl:value-of select="@m_type"/></a>
          </td>
          <td><xsl:value-of select="@time"/></td>
          <td><xsl:value-of select="@m_host"/></td>
          <td><xsl:value-of select="@m_pid"/></td>
          <td><xsl:value-of select="@m_active"/></td>
          <td><xsl:value-of select="@m_active_channels"/></td>
          <td><xsl:value-of select="@m_sampled_events"/></td>
          <td><xsl:value-of select="@m_dropped_events"/></td>
          <td><xsl:value-of select="@m_speedup_requests"/></td>
          <td><xsl:value-of select="@m_slowdown_requests"/></td>
          <td><xsl:value-of select="@m_maximum_channels"/></td>
        </tr>
      </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

</xsl:stylesheet>
