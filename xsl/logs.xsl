<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="domain">
  <script type="text/javascript">
     <xsl:text>
       try {
         document.domain = 'cern.ch'
       } catch(e)
       {}

       function get_text(el)
       {
           if(el.nodeType == 3) return el.nodeValue;
           if(!el.childNodes) return '';
           var result = '';
           for(var i = 0; i &lt; el.childNodes.length; i++) {
               result += get_text(el.childNodes[i]);
           }
           return result;
       }

       function search_rows(row, expr) 
       {
           var regex = new RegExp(expr, 'i');
           var rows = document.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
           for(var i = 0; i &lt; rows.length; i++) {
                  if(regex == '') {
                      rows[i].style.display = 'table-row';
                  } else {
                      var text = row == 0 ? get_text(rows[i].getElementsByTagName('th')[row]) :  get_text(rows[i].getElementsByTagName('td')[row-1]);
                      if(text.search(regex) == -1) {
                        rows[i].style.display = 'none';
                      } else {
                        rows[i].style.display = 'table-row';
                      }
                  }
              }
              return false;
       }

       function do_nothing(e)
       {
	   if (!e) var e = window.event
           alert(window.event);
	   e.cancelBubble = true;
	   if (e.stopPropagation) e.stopPropagation();
           return false;
       }
     </xsl:text>
  </script>
  <script type="text/javascript" src="/tdaq/web_is/lib/sorttable.js"></script>
</xsl:template>

<xsl:template name="style">
   <style type="text/css">
     <xsl:text>
     th, td { font-size: 10pt; }
     thead th { text-align: center; }
     th,td { text-align: left; }
     td.center { text-align: center; }
     td.right { text-align: right; }
     table.sortable thead {
        background-color: #ddd;
        color:#666666;
        font-weight: bold;
        cursor: pointer;
     }
     table { margin-left: 5%; width: 90% }
     table a { cursor: pointer; text-decoration: none; color: black; }
     table a:focus { color: blue; }
     table a:hover { color: blue; }
     tbody tr:nth-child(2n) { background-color: #eee; }
     h1 { border: solid;
       border-width: 1px 2px 2px 1px;
       background-color: #ddd;
       text-align: center;
       width: 90%; margin-left: 5%; 
     }
     form { display: inline; }
     </xsl:text>
   </style>
</xsl:template>

<!-- PARTITIONS -->

<xsl:template match="/partitions">
<html> 
  <head>
  <title>Partitions on <xsl:value-of select="@host"/></title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>

  <body>
    <h1>Partitions on <xsl:value-of select="@host"/></h1>
    <table rules="all" frame="border" class="sortable">

    <thead>
       <tr>
         <th>Partition <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Partition" type="text" autofocus="autofocus"/></form></th>
         <th>Options</th>
       </tr>
    </thead>
    <tbody>
      <xsl:for-each select="p">
        <tr>
           <th>
                <a><xsl:attribute name="href"><xsl:value-of select="../@host"/>/<xsl:value-of select="."/></xsl:attribute><xsl:value-of select="."/></a>
           </th>
           <td class="center">
                <form>
                   <xsl:attribute name="action"><xsl:value-of select="../@host"/>/<xsl:value-of select="."/></xsl:attribute>
                <input type="text" name="name"/><input type="submit" value="Search"/>
		</form>
           </td>
         </tr>
      </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

<xsl:template match="/files">
<html> 
  <head>
  <title>Logfiles for <xsl:value-of select="@partition"/> on <xsl:value-of select="@host"/></title>
  <xsl:call-template name="domain"></xsl:call-template>
  <xsl:call-template name="style"></xsl:call-template>
  </head>

  <body>
    <h1>Logfiles for <xsl:value-of select="@partition"/> on <xsl:value-of select="@host"/></h1>
    <table rules="all" frame="border" class="sortable">

    <thead>
       <tr>
         <th class="sorttable_alpha">Name <form onsubmit="return search_rows(0, this.elements[0].value)"> <input onclick="event.cancelBubble = true;" size="10" placeholder="Filename" type="text" autofocus="autofocus"/></form></th>
         <th class="sorttable_numeric">Size</th>
         <th>Date</th>
         <th class="sorttable_nosort">Options</th>
       </tr>
    </thead>
    <tbody>
      <xsl:for-each select="f">
        <tr>
           <th>
               <a><xsl:attribute name="href"><xsl:value-of select="../@partition"/>/<xsl:value-of select="."/></xsl:attribute><xsl:value-of select="."/></a>
            </th>
           <td class="right"><xsl:value-of select="@size"/></td>
           <td class="center"><xsl:value-of select="@time"/></td>
           <td>
               <form>
                   <xsl:attribute name="action"><xsl:value-of select="../@partition"/>/<xsl:value-of select="."/></xsl:attribute>
                   <label>Head: </label>
                   <input name="head" size="4"/>
                   <label>Tail: </label>
                   <input name="tail" size="4"/>
                   <input type="submit" value="Get"/>
               </form>
           </td>
         </tr>
      </xsl:for-each>
    </tbody>
    </table>
  </body>
</html>
</xsl:template>

</xsl:stylesheet>
